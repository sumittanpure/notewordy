import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import Colours from '../style/colours'
import { globalStyles } from '../style/globalStyles';
import * as Progress from 'react-native-progress';
import {MaterialIcons} from '@expo/vector-icons'
import convertToRoman from './convert_roman';

export default function ListCard({item}){
    return (
        <View style= {styles.list_card}>
            <View style= {styles.square}>
                <Text style= {{fontSize: 32, fontFamily: 'body-bold', color: Colours.primary1}}>{convertToRoman(item.key)}</Text>
            </View>
            <View style= {{flex: 1}}>
                <Text style= {globalStyles.heading2}>{item.name}</Text>
                <Text style= {globalStyles.body1}>{25 - (item.length)}/25 words learnt</Text>
                <View style= {{alignSelf: 'stretch'}}>
                    <Progress.Bar
                        progress={(25-item.length)/25}
                        style= {{alignSelf: 'stretch'}}
                        height= {16}
                        borderRadius= {16}
                        width= {null}
                        color= {Colours.secondary}
                        borderWidth= {0}
                        unfilledColor= {Colours.primary2}
                    />
                </View>
            </View>
            <MaterialIcons name="chevron-right" size={32} color="black"/>
        </View>
    );
}

const styles = StyleSheet.create({
    list_card: {
        marginVertical: 12,
        flexDirection: 'row',
        alignItems: 'center'
    },
    square: {
        height: 88,
        width: 88,
        backgroundColor: Colours.tertiary,
        borderRadius: 16,
        marginRight: 12,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
