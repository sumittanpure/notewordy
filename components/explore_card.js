import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import colours from '../style/colours';

export default function ExploreCard({value, sentence}){
    return (
        <View style= {styles.card}>
            <View style= {{flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-end', alignItems: 'flex-end'}}>
                <Text numberOfLines={1} style= {{color: colours.tertiary, fontFamily: 'oswald-bold', fontSize: 72, opacity: 0.05}}>{value}</Text>
            </View>
            <Text style= {{fontSize: 40, fontFamily: 'oswald-bold', color: colours.tertiary, paddingHorizontal: 16, paddingTop: 16}}>{value}</Text>
            <Text style={{fontSize: 16, fontFamily: 'oswald-regular', color: colours.tertiary, paddingHorizontal: 16, marginTop: -8}}>{sentence}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        backgroundColor: colours.primary1,
        borderRadius: 16,
        overflow: 'hidden',
    },
});