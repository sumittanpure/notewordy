import React, { useState, useEffect } from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import colours from '../style/colours';
import {MaterialIcons} from '@expo/vector-icons'
import { globalStyles } from '../style/globalStyles';


export default function Accordion({item}){
    const [isOpen, setIsOpen] = useState(false);
    useEffect(()=>{
        setIsOpen(false);
    }, [item]);
    if (!isOpen){
        return(
            <TouchableOpacity onPress= {()=> setIsOpen(true)} style= {styles.accordion}>
                <View>
                    <View style= {{flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style= {styles.headerText}>See Meaning</Text>
                    </View>
                    <View style= {{alignSelf: 'flex-end'}}>
                        <MaterialIcons name="keyboard-arrow-down" size={24} color= {colours.primary1}/>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }else{

        if(item.eg2){
            return(
                <View style= {styles.info}>
                    <Text style= {styles.bodySubText}>Meaning</Text>
                    <Text style= {globalStyles.body1}>{item.def}</Text>
                    <Text style= {styles.bodySubText}>Examples</Text>
                    <Text style= {globalStyles.body1}>1. {item.eg1}</Text>
                    <Text style= {globalStyles.body1}>2. {item.eg2}</Text>
                </View>
            );
        }else{
            return(
                <View style= {styles.info}>
                    <Text style= {styles.bodySubText}>Meaning</Text>
                    <Text style= {globalStyles.body1}>{item.def}</Text>
                    <Text style= {styles.bodySubText}>Examples</Text>
                    <Text style= {globalStyles.body1}>1. {item.eg1}</Text>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    accordion: {
        marginTop: 24,
        backgroundColor: colours.tertiary,
        padding: 16,
        alignSelf: 'stretch',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    headerText: {
        fontFamily: 'oswald-bold',
        fontSize: 16,
        color: colours.primary1,
    },
    bodySubText: {
        fontFamily: 'oswald-bold',
        fontSize: 16,
        color: colours.tertiary,
        paddingBottom: 8,
        paddingTop: 24
    },
    info: {
        padding: 16,
        alignSelf: 'stretch',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        alignItems: 'flex-start'
    }
});