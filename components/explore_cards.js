import React from 'react';
import { StyleSheet, View } from 'react-native';
import ExploreCard from './explore_card';

export default function ExploreCards({wordsTotal, wordsToday}){
    return (
      <View style = {styles.cards}>
        <View style= {styles.shadow}>
          <ExploreCard value= {wordsTotal} sentence= {'Words learnt till date'}/>
        </View>
        <View style= {{width: 16}}></View>
        <View style= {styles.shadow}>
          <ExploreCard value= {wordsToday.words} sentence= {'Words learnt today'}/>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
    cards: { 
      flexDirection: 'row',
      justifyContent: 'space-between',
      height: 190,
      paddingVertical: 10,
    },
    shadow: {
      flex: 1,
      borderRadius: 16,
      backgroundColor: 'transparent',
      shadowColor: '#000',
      shadowOpacity: 0.1,
      shadowRadius: 2.22,
      elevation: 20,

    }
});