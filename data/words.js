export const groups = [
    {
        "name": "Section 1",
        "key": "1",
        "length": 25,
        "words": [
            {
                "word": "Laconic",
                "key": 1,
                "type": "adjective",
                "def": "using few words; brief; to the point",
                "eg1": "His laconic reply suggested a lack of interest in the topic.",
                "eg2": "We see him change from being a chubby, loud mouthed drunkard to being a lean, mean, laconic, vengeance machine."
            },
            {
                "word": "Laud",
                "key": 2,
                "type": "verb/noun",
                "def": "praise",
                "eg1": "The obituary lauded(verb) him as a great statesman and soldier.",
                "eg2": "All glory, laud(noun), and honour to the soldiers returning from war!"
            },
            {
                "word": "Timorous",
                "key": 3,
                "type": "adjective",
                "def": "expressing or suggesting timidity; fearful",
                "eg1": "He proceed with doubtful and timorous steps before he asked her out.",
                "eg2": "The timorous rabbit ran away as he heard the wolf howl."
            },
            {
                "word": "Bureaucracy",
                "key": 4,
                "type": "noun",
                "def": "an organization in which action is obstructed by insistence on unnecessary procedures",
                "eg1": "I had to deal with the university's bureaucracy before I could change from one course to another.",
                "eg2": "He described the new regulations as bureaucracy gone mad."
            },
            {
                "word": "Acrophobia",
                "key": 5,
                "type": "noun",
                "def": "fear of heights",
                "eg1": "He decided to go bungee jumping to overcome his acrophobia."
            },
            {
                "word": "Obsolete",
                "key": 6,
                "type": "adjective",
                "def": "no longer valid",
                "eg1": "Please dispose off the old and obsolete machinery.",
                "eg2": "The contract has been deemed obsolete."
            },
            {
                "word": "Abstain",
                "key": 7,
                "type": "verb",
                "def": "restrain oneself from doing or enjoying something",
                "eg1": "Forty-one voted with the Opposition, and some sixty more abstained.",
                "eg2": "Many religious people decide to abstain from something to focus more clearly on God."
            },
            {
                "word": "Haughtiness",
                "key": 8,
                "type": "noun",
                "def": "the quality of being arrogant, having too much pride",
                "eg1": "It was, however, due to his haughty and violent temper that the traditional family relations were disrupted.",
                "eg2": "Haughtiness invites disaster , humility receives benefit."
            },
            {
                "word": "Accolade",
                "key": 9,
                "type": "noun",
                "def": "tribute; honor; praise",
                "eg1": "the hotel has won numerous accolades",
                "eg2": "He's been granted the ultimate accolade - his face on a postage stamp."
            },
            {
                "word": "Harangue",
                "key": 10,
                "type": "noun",
                "def": "a lengthy and aggressive speech",
                "eg1": "He delivered a long harangue about the evils of popular culture.",
                "eg2": "I launched into a long harangue about poor customer service."
            },
            {
                "word": "Histrionic",
                "key": 11,
                "type": "adjective/noun",
                "def": "theatrical; exaggerated",
                "eg1": "He had a histrionic(adjective) outburst.",
                "eg2": "By now, Anna was accustomed to her mother's histrionics(noun)."
            },
            {
                "word": "Covert",
                "key": 12,
                "type": "adjective",
                "def": "hidden; undercover",
                "eg1": "The military oragnised a covert operation to capture the terrotists.",
                "eg2": "They have been supplying covert military aid to the rebels."
            },
            {
                "word": "Temper",
                "key": 13,
                "type": "verb",
                "def": "act as a neutralizing or counterbalancing force to (something).",
                "eg1": "Their idealism is tempered with realism."
            },
            {
                "word": "Aesthetic",
                "key": 14,
                "type": "adjective",
                "def": "concerning art or beauty",
                "eg1": "The new building has an aesthetic appeal.",
                "eg2": "The outfit that she was wearing was giving an aesthetic vibe."
            },
            {
                "word": "Resonant",
                "key": 15,
                "type": "adjective",
                "def": "deep, clear, and continuing to sound or reverberate; echoing",
                "eg1": "The sound of these instruments, played in a resonant room, is unforgettable.",
                "eg2": "The air was resonant with bells on Christmas eve."
            },
            {
                "word": "Hoary",
                "key": 16,
                "type": "adjective",
                "def": "showing characteristics of age; ancient",
                "eg1": "After ages, they finally discussed the hoary old problem.",
                "eg2": "The forest contains many majestic old oaks and hoary willows."
            },
            {
                "word": "Blithe",
                "key": 17,
                "type": "adjective",
                "def": "free-spirited; carefree",
                "eg1": "He had a blithe disregard for the rules of the road.",
                "eg2": "The autumn air , blithe and mellow, made him calm."
            },
            {
                "word": "Headstrong",
                "key": 18,
                "type": "adjective",
                "def": "stubborn; willful",
                "eg1": "She's hopelessly headstrong; she always gets up against people around her.",
                "eg2": "Most people are headstrong and impulsive in their youth."
            },
            {
                "word": "Plaintiff",
                "key": 19,
                "type": "noun",
                "def": "petitioner (in court of law)",
                "eg1": "The plaintiff claimed that the correct procedures had not been followed."
            },
            {
                "word": "Cower",
                "key": 20,
                "type": "verb",
                "def": "recoil, crouch down in fear; shrink away from",
                "eg1": "The children cowered in terror as the shoot-out erupted."
            },
            {
                "word": "Rigor",
                "key": 21,
                "type": "noun",
                "def": "thoroughness",
                "eg1": "There is a need for academic rigor in approaching this problem.",
                "eg2": "I rejected their analysis because it lacks rigor."
            },
            {
                "word": "Credulous",
                "key": 22,
                "type": "adjective",
                "def": "gullible; ready to believe anything",
                "eg1": "Credulous people can often be easy targets for scams.",
                "eg2": "Kara took the joke seriously because she was too credulous to understand the sarcasm."
            },
            {
                "word": "Lax",
                "key": 23,
                "type": "adjective",
                "def": "careless or not strict; relaxed",
                "eg1": "The lax security arrangements at the airport were the cause of the blast.",
                "eg2": "You should lax your muscles after a strenous exercise."
            },
            {
                "word": "Lamentation",
                "key": 24,
                "type": "noun",
                "def": "expression of regret or sorrow",
                "eg1": "The minister's was a time for mourning and lamentation."
            },
            {
                "word": "Reticent",
                "key": 25,
                "type": "adjective",
                "def": "restrained; holding something back; uncommunicative",
                "eg1": "She was extremely reticent about her personal affairs.",
                "eg2": "Most of the students were reticent about answering questions."
            }
        ]
    },
    {
        "name": "Section 2",
        "key": "2",
        "length": 25,
        "words": [
            {
                "word": "Tawdry",
                "key": 1,
                "type": "adjective",
                "def": "showy but cheap and of poor quality",
                "eg1": "She wears tawdry jewellery.",
                "eg2": null
            },
            {
                "word": "Blatant",
                "key": 2,
                "type": "adjective",
                "def": "done openly and unashamedly, obvious",
                "eg1": "Despite their blatant attraction to each other they try to stay just friends.",
                "eg2": "blatant lies;"
            },
            {
                "word": "Bombast",
                "key": 3,
                "type": "noun",
                "def": "arrogant, pompous language",
                "eg1": "Politicians's speeches are usually full of bombast.",
                "eg2": "This is a singular prose poem, in language sometimes rather bombastic but often beautiful."
            },
            {
                "word": "Obtuse",
                "key": 4,
                "type": "adjective",
                "def": "insensitive or slow to understand; mentally dull",
                "eg1": "He wondered if the students were being deliberately obtuse.",
                "eg2": "Perhaps I'm being obtuse, but what has all this got to do with me?"
            },
            {
                "word": "Poised",
                "key": 5,
                "type": "adjective",
                "def": "calm; collected; self-possessed",
                "eg1": "It is not every day that you see a poised, competent kid distressed.",
                "eg2": "The eagle poised in mid-air ready to swoop on its prey."
            },
            {
                "word": "Lance",
                "key": 6,
                "type": "noun",
                "def": "spear; spike; javelin",
                "eg1": "The horseman hurled a lance at his target."
            },
            {
                "word": "Absolution",
                "key": 7,
                "type": "noun",
                "def": "forgiveness; pardon; release",
                "eg1": "She had been granted absolution for her sins."
            },
            {
                "word": "Placid",
                "key": 8,
                "type": "adjective",
                "def": "calm, peaceful",
                "eg1": "The placid waters of Kashmir make for beautiful pictures.",
                "eg2": "Built in 1960, the facility is now as well known for its placid, soaring architecture."
            },
            {
                "word": "Hedonist",
                "key": 9,
                "type": "noun",
                "def": "a pleasure seeker",
                "eg1": "She was living the life of a committed hedonist."
            },
            {
                "word": "Debunking",
                "key": 10,
                "type": "verb",
                "def": "exposing false claims or myths",
                "eg1": "At her seminar, she debunked all the allegations thrown at her.",
                "eg2": "His theories have been debunked by recent research."
            },
            {
                "word": "Broach",
                "key": 11,
                "type": "verb",
                "def": "raise (a difficult subject) for discussion.",
                "eg1": "He broached the subject he had been avoiding all evening.",
                "eg2": "The report fails to broach some important questions."
            },
            {
                "word": "Reprieve",
                "key": 12,
                "type": "verb/noun",
                "def": "cancel or postpone the punishment of (someone)",
                "eg1": "Under the new regime, prisoners under sentence of death were reprieved(verb).",
                "eg2": "He accepted the death sentence and refused to appeal for a reprieve(noun)."
            },
            {
                "word": "Braggart",
                "key": 13,
                "type": "noun",
                "def": "someone who boasts",
                "eg1": "He is a braggart who is always talking about how much money he makes."
            },
            {
                "word": "Tangent",
                "key": 14,
                "type": "noun",
                "def": "going off the main subject",
                "eg1": "It surprised everyone when our history teacher went off on a tangent about physics",
                "eg2": "a tangential topic;"
            },
            {
                "word": "Defoliate",
                "key": 15,
                "type": "verb",
                "def": "cause leaves to fall off",
                "eg1": "The area was defoliated and napalmed many times."
            },
            {
                "word": "Affable",
                "key": 16,
                "type": "adjective",
                "def": "friendly; social; easygoing",
                "eg1": "He is an affable and agreeable companion.",
                "eg2": "Granny calmed everyone with her affable smile."
            },
            {
                "word": "Epitomized",
                "key": 17,
                "type": "verb",
                "def": "typified; characterized; personified",
                "eg1": "The company epitomized the problems faced by Indian industry.",
                "eg2": "Cristiano Ronaldo epitomised the team's never-say-die spirit, and eventually won the cup."
            },
            {
                "word": "Obsequious",
                "key": 18,
                "type": "adjective",
                "def": "obedient or attentive to an excessive or servile degree",
                "eg1": "They were served by obsequious waiters.",
                "eg2": "She is almost embarrassingly obsequious to anyone in authority."
            },
            {
                "word": "Tentative",
                "key": 19,
                "type": "adjective",
                "def": "not certain",
                "eg1": "He reached some tentative conclusions about the possible cause of the accident.",
                "eg2": "The country is taking its first tentative steps towards democracy."
            },
            {
                "word": "Obdurate",
                "key": 20,
                "type": "adjective",
                "def": "stubborn",
                "eg1": "I argued this point with him, but he was obdurate",
                "eg2": "The President remains obdurate on the question of tax cuts."
            },
            {
                "word": "Languid",
                "key": 21,
                "type": "adjective",
                "def": "tired; slow (not necessarily negative)",
                "eg1": "She was pale, languid, and weak after delivering a child.",
                "eg2": "We spent a languid afternoon by the pool."
            },
            {
                "word": "Tedium",
                "key": 22,
                "type": "noun",
                "def": "boredom, tediousness",
                "eg1": "Oh, the tedium of car journeys!",
                "eg2": "We sang while we worked, to relieve the tedium ."
            },
            {
                "word": "Billowing",
                "key": 23,
                "type": "adjective",
                "def": "swelling; fluttering; waving",
                "eg1": "The national flag is billowing in the breeze.",
                "eg2": "All I could see was thick, billowing smoke."
            },
            {
                "word": "Poseur",
                "key": 24,
                "type": "noun",
                "def": "a person who habitually pretends to be something he is not",
                "eg1": "You look like a real poseur in your fancy sports car with your expensive clothes!"
            },
            {
                "word": "Repudiate",
                "key": 25,
                "type": "verb",
                "def": "refuse to accept; reject",
                "eg1": "The company will repudiate any claims of negligence.",
                "eg2": "During his next speech, the president will repudiate blame for the economic situation."
            }
        ]
    },
    {
        "name": "Section 3",
        "key": "3",
        "length": 25,
        "words": [
            {
                "word": "Oblivious",
                "key": 1,
                "type": "adjective",
                "def": "totally unaware",
                "eg1": "He should have heeded(verb) the warnings before it was too late.",
                "eg2": "He seemed oblivious to the fact that he had hurt her."
            },
            {
                "word": "Enunciation",
                "key": 2,
                "type": "verb",
                "def": "express (a proposition, theory, etc.) in clear or definite terms",
                "eg1": "Give me a written document enunciating this policy.",
                "eg2": "She enunciated each word slowly."
            },
            {
                "word": "Plumage",
                "key": 3,
                "type": "noun",
                "def": "feathers of a bird",
                "eg1": "He likes birds with exotic plumage.",
                "eg2": "The bright plumage of many male birds has evolved to attract females."
            },
            {
                "word": "Hidebound",
                "key": 4,
                "type": "adjective",
                "def": "rigid in opinions; unwilling to accept change",
                "eg1": "They are working to change hidebound corporate cultures.",
                "eg2": "Life should not be hidebound by superstition or convention."
            },
            {
                "word": "Abstemious",
                "key": 5,
                "type": "adjective",
                "def": "refraining from indulging in",
                "eg1": "Very abstemious of you to have only one drink.",
                "eg2": "He is abstemious in eating and drinking."
            },
            {
                "word": "Respite",
                "key": 6,
                "type": "verb/noun",
                "def": "a short period of rest or relief from something difficult or unpleasant; postpone",
                "eg1": "The refugee encampments will provide some respite(noun) from the suffering.",
                "eg2": "The execution was only respited(verb) a few months."
            },
            {
                "word": "Blasphemy",
                "key": 7,
                "type": "noun",
                "def": "speech which offends religious sentiments",
                "eg1": "He was detained on charges of blasphemy."
            },
            {
                "word": "Enhance",
                "key": 8,
                "type": "verb",
                "def": "intensify, increase, or further improve the quality or value of",
                "eg1": "Good lighting will enhance any room.",
                "eg2": "The king wanted to enhance his prestige through war."
            },
            {
                "word": "Hypochondriac",
                "key": 9,
                "type": "noun",
                "def": "a person obsessed with health; having imaginary illnesses",
                "eg1": "Don't be such a hypochondriac!\u2014there's nothing wrong with you."
            },
            {
                "word": "Hedonism",
                "key": 10,
                "type": "noun",
                "def": "self indulgence; pleasure-seeking",
                "eg1": "Instead, the hedonism of house has been channelled into an enjoyable debut."
            },
            {
                "word": "Nuance",
                "key": 11,
                "type": "noun",
                "def": "something subtle; a fine shade of meaning",
                "eg1": "He was familiar with the nuances of the local dialect.",
                "eg2": "Because she is a chef, she notices every nuance of flavor in the meal"
            },
            {
                "word": "Labyrinthine",
                "key": 12,
                "type": "adjective",
                "def": "intricate and confusing.",
                "eg1": "They devised labyrinthine plots and counterplots for the mission.",
                "eg2": "The streets of the Old City are narrow and labyrinthine."
            },
            {
                "word": "Hamper",
                "key": 13,
                "type": "verb(used with an object)",
                "def": "hinder, obstruct",
                "eg1": "A steady rain hampered the progress of the work.",
                "eg2": "The dancers' movements were hampered by their elaborate costumes."
            },
            {
                "word": "Lachrymose",
                "key": 14,
                "type": "adjective",
                "def": "tearful; sad",
                "eg1": "a lachrymose children's classic;",
                "eg2": "After my uncle died, my aunt became a lachrymose woman wo wouldn't stop crying."
            },
            {
                "word": "Creditable",
                "key": 15,
                "type": "adjective",
                "def": "praiseworthy",
                "eg1": "A creditable achievement."
            },
            {
                "word": "Obscure",
                "key": 16,
                "type": "adjective/verb",
                "def": "difficult to understand; partially hidden",
                "eg1": "his origins and parentage are obscure(adjective).",
                "eg2": "Grey clouds obscure the sun(verb)."
            },
            {
                "word": "Ensconce",
                "key": 17,
                "type": "verb",
                "def": "establish or settle (someone) in a comfortable, safe place.",
                "eg1": "Agnes ensconced herself in their bedroom."
            },
            {
                "word": "Larceny",
                "key": 18,
                "type": "noun",
                "def": "theft; robbery; stealing",
                "eg1": "The man was imprisoned for larceny.",
                "eg2": "Issuing a bad check is a form of larceny."
            },
            {
                "word": "Erratic",
                "key": 19,
                "type": "adjective",
                "def": "not even or regular in pattern or movement; unpredictable",
                "eg1": "The weather prediction for the next week is very erratic.",
                "eg2": "The movement of the stock market has been very erratic."
            },
            {
                "word": "Enshroud",
                "key": 20,
                "type": "verb",
                "def": "envelop completely and hide from view; cover",
                "eg1": "Heavy grey clouds enshrouded the city.",
                "eg2": "The monument was enshrouded with flags, candles, flowers and other mementos."
            },
            {
                "word": "Envenom",
                "key": 21,
                "type": "verb",
                "def": "to cause bitterness and bad feeling",
                "eg1": "Tribal rivalries envenomed the bitter civil war.",
                "eg2": "He committed thoughtless, self-indulgent antics that only managed to envenom his teammates."
            },
            {
                "word": "Brusque",
                "key": 22,
                "type": "adjective",
                "def": "blunt; abrupt",
                "eg1": "His secretary was rather brusque with me.",
                "eg2": "His brusque manner hides a shy and sensitive nature."
            },
            {
                "word": "Degradation",
                "key": 23,
                "type": "noun",
                "def": "deprivation; poverty; debasement",
                "eg1": "I feel these actions signal the degradation of our culture.",
                "eg2": "Environmental degradation is an outcome of deforestation."
            },
            {
                "word": "Resignation",
                "key": 24,
                "type": "noun",
                "def": "the acceptance of something undesirable but inevitable.",
                "eg1": "After last week's events, he announced his resignation.",
                "eg2": "They received the awful news with a shrug of resignation."
            },
            {
                "word": "Debility",
                "key": 25,
                "type": "noun",
                "def": "weakness; incapacity",
                "eg1": "Most cases of COVID presented with general debility and muscle weakness.",
                "eg2": "After her long illness she is suffering from general debility."
            }
        ]
    },
    {
        "name": "Section 4",
        "key": "4",
        "length": 25,
        "words": [
            {
                "word": "Acrid",
                "key": 1,
                "type": "adjective",
                "def": "unpleasantly bitter or pungent.",
                "eg1": "Clouds of acrid smoke issued from the building.",
                "eg2": "The fog was yellow and acrid and bit at the back of the throat."
            },
            {
                "word": "Largesse",
                "key": 2,
                "type": "noun",
                "def": "generosity(mainly in bestowing gifts or material goods)",
                "eg1": "Public money is not dispensed with such largesse in any other country.",
                "eg2": "Mr. King liked the lordliness of giving largess and was thus very popular."
            },
            {
                "word": "Plaudit",
                "key": 3,
                "type": "noun",
                "def": "an expression of praise or approval.",
                "eg1": "The network has received plaudits for its sports coverage."
            },
            {
                "word": "Adversity",
                "key": 4,
                "type": "noun",
                "def": "hardship;a difficult or unpleasant situation",
                "eg1": "He showed resilience in the face of adversity.",
                "eg2": "You understand who your real friends are in the face of adversity."
            },
            {
                "word": "Nullify",
                "key": 5,
                "type": "verb",
                "def": "to counter; make unimportant",
                "eg1": "It is at the discretion of the court to nullify the decision.",
                "eg2": "Junk food nullifies the effect of exercise."
            },
            {
                "word": "Hieroglyphics",
                "key": 6,
                "type": "noun",
                "def": "1. picture writing of ancient times 2. writing which is difficult to read or enigmatic",
                "eg1": "Without her expertise, it is likely that Egyptian hieroglyphics would still be a mystery.",
                "eg2": "No one can understand his notebooks as they are filled with illegible hieroglyphics."
            },
            {
                "word": "Renown",
                "key": 7,
                "type": "noun",
                "def": "fame",
                "eg1": "He achieved great renown for his discoveries.",
                "eg2": "Her photographs have earned her international renown."
            },
            {
                "word": "Remuneration",
                "key": 8,
                "type": "noun",
                "def": "payment for the work done",
                "eg1": "He received little remuneration for his services."
            },
            {
                "word": "Portent",
                "key": 9,
                "type": "noun",
                "def": "a warning sign; omen",
                "eg1": "Some people believe the raven is a portent of death.",
                "eg2": "The event proved to be a portent of the disaster that was to come."
            },
            {
                "word": "Credible",
                "key": 10,
                "type": "adjective",
                "def": "believable",
                "eg1": "Hardly anyone found his story credible",
                "eg2": "The challenge before the opposition is to offer credible alternative policies for the future."
            },
            {
                "word": "Languish",
                "key": 11,
                "type": "verb",
                "def": "decay; fade away; get weaker",
                "eg1": "The children soon began to languish in the heat.",
                "eg2": "She continues to languish in a foreign prison."
            },
            {
                "word": "Therapeutic",
                "key": 12,
                "type": "adjective",
                "def": "medicinal; curative",
                "eg1": "The music he played on the flute had a therapeutic effect on the audience.",
                "eg2": "Some claim that the herb has therapeutic value for treating pain."
            },
            {
                "word": "Riddled",
                "key": 13,
                "type": "adjective",
                "def": "full of (usually full of holes)",
                "eg1": "The door of the fort was riddled with bullets.",
                "eg2": "The car was riddled with bullets."
            },
            {
                "word": "Oblique",
                "key": 14,
                "type": "adjective",
                "def": "indirect; slanting",
                "eg1": "He issued an oblique attack on the President.",
                "eg2": "We sat on the seat oblique to the fireplace."
            },
            {
                "word": "Brevity",
                "key": 15,
                "type": "noun",
                "def": "concise and exact use of words in writing or speech; being brief",
                "eg1": "The brevity of the concert disappointed the audience.",
                "eg2": "His essays are models of brevity and clarity."
            },
            {
                "word": "Totter",
                "key": 16,
                "type": "verb",
                "def": "walk/move in an unsteadily manner",
                "eg1": "A hunched figure was tottering down the path.",
                "eg2": "The building began to totter and then the roof gave way."
            },
            {
                "word": "Espouse",
                "key": 17,
                "type": "verb",
                "def": "1) adopt or support (a cause, belief, or way of life); 2) marry",
                "eg1": "The prime minister espoused the causes of justice and freedom for all.",
                "eg2": "Edward had espoused the lady Grey."
            },
            {
                "word": "Etymology",
                "key": 18,
                "type": "noun",
                "def": "the study of word origins and developments",
                "eg1": "At university she developed an interest in etymology."
            },
            {
                "word": "Odious",
                "key": 19,
                "type": "adjective",
                "def": "hateful; extremely unpleasant",
                "eg1": "The taking of hostages is an odious crime.",
                "eg2": "He has been portrayed as a pretty odious character."
            },
            {
                "word": "Equivocate",
                "key": 20,
                "type": "verb",
                "def": "use ambiguous language so as to conceal the truth or avoid committing oneself",
                "eg1": "Whenever he is asked about his girlfriend, he always gives equivocal replies.",
                "eg2": "The government have equivocated too often in the past when asked about Area 51."
            },
            {
                "word": "Noxious",
                "key": 21,
                "type": "adjective",
                "def": "harmful, poisonous, lethal",
                "eg1": "The noxious fumes produced during the fire, killed many.",
                "eg2": "It is a noxious and outrageous notion that insults the intellectual abilities of black students."
            },
            {
                "word": "Tome",
                "key": 22,
                "type": "noun",
                "def": "a book, especially a large, heavy, scholarly one",
                "eg1": "She insists the tome is pure fiction."
            },
            {
                "word": "Hangar",
                "key": 23,
                "type": "noun",
                "def": "a large building with an extensive floor area, typically for housing aircraft.",
                "eg1": "The army choppers went nose first into the hangar."
            },
            {
                "word": "Acquiesce",
                "key": 24,
                "type": "verb",
                "def": "accept something reluctantly but without protest.",
                "eg1": "Sara acquiesced in his decision eventhough she did not support it.",
                "eg2": "Her parents will never acquiesc in this marriage."
            },
            {
                "word": "Enfranchise",
                "key": 25,
                "type": "verb",
                "def": "to set free, give voting rigths",
                "eg1": "North Dakota's constitution provided that the legislature might in the future enfranchise women."
            }
        ]
    },
    {
        "name": "Section 5",
        "key": "5",
        "length": 25,
        "words": [
            {
                "word": "Acuity",
                "key": 1,
                "type": "noun",
                "def": "sharpness (mental or visual)",
                "eg1": "A motorist needs good visual acuity.",
                "eg2": "As babies get in the five month to one year range, they can see objects farther away and with more acuity."
            },
            {
                "word": "Hiatus",
                "key": 2,
                "type": "noun",
                "def": "interruption; pause",
                "eg1": "There was a brief hiatus in the war with France.",
                "eg2": "The manager took a hiatus to get his head straight."
            },
            {
                "word": "Adamant",
                "key": 3,
                "type": "adjective",
                "def": "forceful; inflexible",
                "eg1": "The prime minister is adamant that he will not resign.",
                "eg2": "She remains adamant that an injustice was done."
            },
            {
                "word": "Engender",
                "key": 4,
                "type": "verb",
                "def": "cause or give rise to (a feeling, situation, or condition)",
                "eg1": "The issue engendered the continuing controversy."
            },
            {
                "word": "Lackluster",
                "key": 5,
                "type": "adjective",
                "def": "dull; monotonous; bland",
                "eg1": "No excuses were made for the team's lacklustre performance.",
                "eg2": "The conditioner will revitalize lacklustre hair."
            },
            {
                "word": "Hinder",
                "key": 6,
                "type": "verb",
                "def": "obstruct; restrict",
                "eg1": "Language barriers hindered communication between the tourists.",
                "eg2": "Tight clothes tend to hinder movement."
            },
            {
                "word": "Tardy",
                "key": 7,
                "type": "adjective",
                "def": "slow; late; overdue; delayed",
                "eg1": "The agency was heavily criticised for its tardy response to the hurricane.",
                "eg2": "Please forgive my tardy reply."
            },
            {
                "word": "Bolster",
                "key": 8,
                "type": "verb",
                "def": "support or strengthen; prop up",
                "eg1": "The team's performance today has bolstered everyone's confidence.",
                "eg2": "Falling interest rates may help to bolster up the economy."
            },
            {
                "word": "Retention",
                "key": 9,
                "type": "noun",
                "def": "preservation; withholding",
                "eg1": "This soil has a great capacity for water retention.",
                "eg2": "Managers are responsible for the safe custody and retention of records."
            },
            {
                "word": "Curtail",
                "key": 10,
                "type": "verb",
                "def": "reduce in extent or quantity; impose a restriction on.",
                "eg1": "Civil liberal rights were further curtailed after the dictator took over.",
                "eg2": "I don't want to curtail my daughter's freedom."
            },
            {
                "word": "Hone",
                "key": 11,
                "type": "verb",
                "def": "refine or perfect (something) over a period of time",
                "eg1": "Some of the best players in the world honed their skills playing street football.",
                "eg2": "He runs every day to hone his stamina."
            },
            {
                "word": "Revere",
                "key": 12,
                "type": "verb",
                "def": "regard with feelings of respect and reverence",
                "eg1": "Most of us revere the Bible, but few of us read it regularly.",
                "eg2": "Students revere the old professors."
            },
            {
                "word": "Legend",
                "key": 13,
                "type": "noun",
                "def": "a traditional story sometimes popularly regarded as historical but not authenticated.",
                "eg1": "According to ancient legend, the river is a goddess.",
                "eg2": "According to ancient legend, the river goddess eats people."
            },
            {
                "word": "Counterfeit",
                "key": 14,
                "type": "adjective",
                "def": "fake; false",
                "eg1": "The photos are then analyzed by in-house experts who determine whether the goods are genuine or counterfeit.",
                "eg2": "Governmets ban the use of counterfeit bills."
            },
            {
                "word": "Rife",
                "key": 15,
                "type": "adjective",
                "def": "of common occurrence; widespread(especially of something undesirable)",
                "eg1": "Corruption was rife during the election.",
                "eg2": "Dysentery and malaria are rife in the refugee camps."
            },
            {
                "word": "Plummet",
                "key": 16,
                "type": "verb/noun",
                "def": "fall suddenly and steeply",
                "eg1": "A climber was killed when he plummeted(verb) 300 feet down an icy gully.",
                "eg2": "The bird has a circular display flight followed by an earthward plummet(noun)."
            },
            {
                "word": "Nuzzle",
                "key": 17,
                "type": "verb",
                "def": "rub or push against gently with the nose and mouth",
                "eg1": "The dog nuzzled her hair."
            },
            {
                "word": "Posthumous",
                "key": 18,
                "type": "adjective",
                "def": "after death",
                "eg1": "He received a posthumous award for bravery.",
                "eg2": "A posthumous collection of her work has just been published."
            },
            {
                "word": "Resolution",
                "key": 19,
                "type": "noun",
                "def": "the quality of being determined or resolute",
                "eg1": "He handled the last actions of the war with the same resolution as the first.",
                "eg2": "She cracked the examination due to her resolution towards her studies."
            },
            {
                "word": "Crepuscular",
                "key": 20,
                "type": "adjective",
                "def": "active at dawn and dusk",
                "eg1": "Bats are crepuscular creatures."
            },
            {
                "word": "Kindle",
                "key": 21,
                "type": "verb(used with an object)",
                "def": "to start a fire; to ligth up; illuminate",
                "eg1": "He kindled their hopes of victory.",
                "eg2": "Happiness kindled her eyes."
            },
            {
                "word": "Knotty",
                "key": 22,
                "type": "adjective",
                "def": "complex; difficult to solve",
                "eg1": "A knotty legal problem.",
                "eg2": "a knotty relationship;"
            },
            {
                "word": "Obstreperous",
                "key": 23,
                "type": "adjective",
                "def": "noisy and boisterous; difficult to control",
                "eg1": "He becomes obstreperous when he's had a few drinks."
            },
            {
                "word": "Torpor",
                "key": 24,
                "type": "noun",
                "def": "dormancy; sluggishness; inactivity",
                "eg1": "She tried to rouse him from the torpor into which he had sunk after his wife died.",
                "eg2": "In the heat, the farmers sank into a state of torpor."
            },
            {
                "word": "Titter",
                "key": 25,
                "type": "verb/noun",
                "def": "giggle quietly",
                "eg1": "Her stutter caused the children to titter(verb).",
                "eg2": "During the play, there were constant titters(noun) from the gallery."
            }
        ]
    },
    {
        "name": "Section 6",
        "key": "6",
        "length": 25,
        "words": [
            {
                "word": "Tenuous",
                "key": 1,
                "type": "adjective",
                "def": "very weak or slight; insubstatial",
                "eg1": "His tenuous grasp on reality affected his state of mine.",
                "eg2": "The difference, if it exists, is extremely tenuous."
            },
            {
                "word": "Officious",
                "key": 2,
                "type": "adjective",
                "def": "domineering; intrusive; meddlesome",
                "eg1": "We were tired of being pushed around by officious family members.",
                "eg2": "The people at the tax department were very officious, and kept everyone waiting for hours while they checked their papers."
            },
            {
                "word": "Blunderbuss",
                "key": 3,
                "type": "noun",
                "def": "an action or way of doing something regarded as lacking in subtlety and precision",
                "eg1": "He usually makes wrong moves while playong chess and hence loses. He is what we would call a blunderbuss."
            },
            {
                "word": "Defunct",
                "key": 4,
                "type": "adjective",
                "def": "no longer in existence",
                "eg1": "The now defunct house is used as a storehouse.",
                "eg2": "He wrote many articles for the now defunct newspaper."
            },
            {
                "word": "Err",
                "key": 5,
                "type": "verb",
                "def": "make a mistake",
                "eg1": "The judge had erred in ruling and was therefore suspended.",
                "eg2": "It is the nature of every man to err, but we must learn from our mistakes."
            },
            {
                "word": "Obviate",
                "key": 6,
                "type": "verb",
                "def": "avoid; make unnecessary",
                "eg1": "A peaceful solution would obviate the need to send a UN military force.",
                "eg2": "A list made beforehand should obviate the necessity for last moment hustle."
            },
            {
                "word": "Decorum",
                "key": 7,
                "type": "noun",
                "def": "dignified, correct behavior; etiquette",
                "eg1": "He acted with the utmost decorum when he met the prime minister.",
                "eg2": "He had no idea of funeral decorum as it was the first funeral he was attending."
            },
            {
                "word": "Obfuscate",
                "key": 8,
                "type": "verb",
                "def": "deliberately make something difficult to understand",
                "eg1": "The new rule is more likely to obfuscate people than enlighten them.",
                "eg2": "She was criticized for using arguments that obfuscated the main issue."
            },
            {
                "word": "Brawny",
                "key": 9,
                "type": "adjective",
                "def": "muscular",
                "eg1": "Everyone is scared of Chinmay because he is a brawny person.",
                "eg2": "They pictured Soviet women as hammer-throwers, brawny six-footers who work in brick factories."
            },
            {
                "word": "Esoteric",
                "key": 10,
                "type": "adjective",
                "def": "obscure and difficult to understand",
                "eg1": "Calculus I esoteric to many.",
                "eg2": "The club is known for esoteric philosophical debates."
            },
            {
                "word": "Thwart",
                "key": 11,
                "type": "verb",
                "def": "prevent; frustrate",
                "eg1": "He never did anything to thwart his father but was still punished.",
                "eg2": "We must thwart his malevolent and evil schemes."
            },
            {
                "word": "Plausible",
                "key": 12,
                "type": "adjective",
                "def": "seeming reasonable or probable",
                "eg1": "Can you offer a plausible explanation for the accident?",
                "eg2": "He did not think it plausible that all the differences could be explained in this way."
            },
            {
                "word": "Epicure",
                "key": 13,
                "type": "noun",
                "def": "someone who appreciates good food and drink",
                "eg1": "This cookery book has been written by a real epicure."
            },
            {
                "word": "Obliterate",
                "key": 14,
                "type": "verb",
                "def": "destroy; demolish; eradicate",
                "eg1": "The memory was so painful that he obliterated it from his mind.",
                "eg2": "Clouds were darkening, obliterating the sun."
            },
            {
                "word": "Tirade",
                "key": 15,
                "type": "verb",
                "def": "a long, angry speech of criticism or accusation",
                "eg1": "He hurled a tirade of abuse at his wife.",
                "eg2": "She launched into a tirade of abuse against politicians."
            },
            {
                "word": "Labyrinth",
                "key": 16,
                "type": "noun",
                "def": "a maze",
                "eg1": "You will lose yourself in a labyrinth of little streets if you do not follow the map."
            },
            {
                "word": "Harbingers",
                "key": 17,
                "type": "noun",
                "def": "indicators of something; bringers of warnings",
                "eg1": "The November air stung my cheeks, a harbinger of winter."
            },
            {
                "word": "Epistle",
                "key": 18,
                "type": "noun",
                "def": "a letter (form of communication)",
                "eg1": "The climate change activists are firing off angry epistles at the government.",
                "eg2": "St Paul's Epistle to the Romans."
            },
            {
                "word": "Ponderous",
                "key": 19,
                "type": "adjective",
                "def": "weighty; slow and heavy (person or thing)",
                "eg1": "The ponderous reporting style makes the evening news dull viewing.",
                "eg2": "The fat woman's movements were ponderous."
            },
            {
                "word": "Laceration",
                "key": 20,
                "type": "noun",
                "def": "a cut",
                "eg1": "He suffered lacerations to his head and face."
            },
            {
                "word": "Rescind",
                "key": 21,
                "type": "verb",
                "def": "retract; repeal",
                "eg1": "My parents agreed to rescind my night crufew.",
                "eg2": "The court has the power to rescind a bankruptcy order."
            },
            {
                "word": "Pliable",
                "key": 22,
                "type": "adjective",
                "def": "flexible(regarding person or substance); not stubborn; easily influenced",
                "eg1": "Teenage minds are usually pliable.",
                "eg2": "Quality leather is pliable and will not crack."
            },
            {
                "word": "Bristle",
                "key": 23,
                "type": "verb",
                "def": "be covered with or abundant in",
                "eg1": "The roof bristled with antennae.",
                "eg2": "His lies made her bristle with rage."
            },
            {
                "word": "Retraction",
                "key": 24,
                "type": "noun",
                "def": "withdrawal; cancellation of a statement",
                "eg1": "He issued a formal retraction of his allegations against the thief.",
                "eg2": "The newspaper printed a retraction for their previous error."
            },
            {
                "word": "Epistolary",
                "key": 25,
                "type": "adjective",
                "def": "concerned with letters; through correspondence",
                "eg1": "He wrote a unique, epistolary novel regarding Anne Frank's letters."
            }
        ]
    },
    {
        "name": "Section 7",
        "key": "7",
        "length": 25,
        "words": [
            {
                "word": "Boorish",
                "key": 1,
                "type": "adjective",
                "def": "rough and ill-mannered",
                "eg1": "Patrick's boorish rudeness soon drove his friends away.",
                "eg2": "He is boorish and rash , so he always brings trouble to his family."
            },
            {
                "word": "Hyperbole",
                "key": 2,
                "type": "noun",
                "def": "grossly exaggerated speech",
                "eg1": "Stand up comedy involves a lot of hyperbole to keep the audience engaged.",
                "eg2": "It was not hyperbole to call it the worst storm in twenty years."
            },
            {
                "word": "Talisman",
                "key": 3,
                "type": "noun",
                "def": "lucky charm",
                "eg1": "The 24-year-old could be the latest talisman for Brazil\u2019s famed samba style of football.",
                "eg2": "He wore a glowing talisman on a leather chain around his neck"
            },
            {
                "word": "Podium",
                "key": 4,
                "type": "noun/verb",
                "def": "raised platform(noun); finish first, second, or third, so as to appear on a podium to receive an award(verb)",
                "eg1": "He was at the podium facing an expectant conference crowd.",
                "eg2": "He received a podium finish at the olympics."
            },
            {
                "word": "Terse",
                "key": 5,
                "type": "adjective",
                "def": "concise; to the point",
                "eg1": "His terse reply ended the conversation without further adieu.",
                "eg2": "He issued a terse statement, saying he is discussing his future with colleagues before announcing his decision on Monday."
            },
            {
                "word": "Replete",
                "key": 6,
                "type": "adjective",
                "def": "filled or well-supplied with something",
                "eg1": "The Harbor was replete with boats.",
                "eg2": "History is replete with examples of populations out of control."
            },
            {
                "word": "Lavish",
                "key": 7,
                "type": "adjective/verb",
                "def": "sumptuously rich, elaborate, or luxurious(adjective); bestow generous or extravagant quantities on(verb)",
                "eg1": "The king hosted a lavish(adjective) banquet.",
                "eg2": "The media couldn't lavish(verb) enough praise on the film."
            },
            {
                "word": "Abasement",
                "key": 8,
                "type": "noun",
                "def": "humiliation; degradation",
                "eg1": "Only an evil man would delight in the abasement of his children",
                "eg2": "The school bullies laughed at his abasement."
            },
            {
                "word": "Abhor",
                "key": 9,
                "type": "verb",
                "def": "hate, regard with hatred",
                "eg1": "Investors abhor inflation because it makes their long-term assets worth less."
            },
            {
                "word": "Hasten",
                "key": 10,
                "type": "verb",
                "def": "be quick to do something, move or travel hurriedly",
                "eg1": "After our trip, we hastened back to Paris.",
                "eg2": "This tragedy probably hastened his own death from heart disease."
            },
            {
                "word": "Ephemeral",
                "key": 11,
                "type": "adjective",
                "def": "short-lived; short-lasting",
                "eg1": "Fashions are ephemeral: new ones regularly drive out the old.",
                "eg2": "Slang words are often ephemeral."
            },
            {
                "word": "Pontificate",
                "key": 12,
                "type": "verb",
                "def": "speak pompously or dogmatically",
                "eg1": "There are lots of people in Washington who pontificate on the issue of the day.",
                "eg2": "Politicians like to pontificate about falling standards."
            },
            {
                "word": "Cryptic",
                "key": 13,
                "type": "adjective",
                "def": "puzzling; enigmatic",
                "eg1": "She made a cryptic comment about how the film mirrored her life.",
                "eg2": "My father's notes are more cryptic and difficult to decipher than I thought."
            },
            {
                "word": "Advocate",
                "key": 14,
                "type": "verb",
                "def": "support; push for something",
                "eg1": "Many people advocate building more libraries.",
                "eg2": "The group does not advocate the use of violence."
            },
            {
                "word": "Posterity",
                "key": 15,
                "type": "noun",
                "def": "future generations",
                "eg1": "Their music has been preserved for posterity.",
                "eg2": "His work must be preserved for posterity."
            },
            {
                "word": "Cringe",
                "key": 16,
                "type": "verb",
                "def": "recoil; flinch; shy away",
                "eg1": "He cringed away from the blow and saved his head.",
                "eg2": "I cringed when I realized what I'd said."
            },
            {
                "word": "Portend",
                "key": 17,
                "type": "verb",
                "def": "foretell; be a sign or warning",
                "eg1": "Rising infection rates portend a health-care disaster.",
                "eg2": "These clouds are ominous. They portend a severe storm."
            },
            {
                "word": "Bulwark",
                "key": 18,
                "type": "noun",
                "def": "fortification; barricade; wall",
                "eg1": "Our people's support is a bulwark against the enemy.",
                "eg2": "My savings were to be a bulwark in case of unemployment."
            },
            {
                "word": "Heresy",
                "key": 19,
                "type": "noun",
                "def": "against orthodox opinion(religious or otherwise)",
                "eg1": "He was burned for heresy against the church.",
                "eg2": "Her belief that taxes should be hugher was heresy."
            },
            {
                "word": "Plethora",
                "key": 20,
                "type": "noun",
                "def": "a large or excessive amount of something",
                "eg1": "The government comprises of a plethora of committees and subcommittees.",
                "eg2": "I have a plethora of shirts, so I will donate some."
            },
            {
                "word": "Abstruse",
                "key": 21,
                "type": "adjective",
                "def": "difficult to understand; obscure",
                "eg1": "Einstein's theory of relativity is very abstruse.",
                "eg2": "Chess playing is a game of such abstruse and dazzling complexity."
            },
            {
                "word": "Bilk",
                "key": 22,
                "type": "verb",
                "def": "cheat; swindler",
                "eg1": "An apparently kind elderly gentleman bilked me of twenty dollars."
            },
            {
                "word": "Torpid",
                "key": 23,
                "type": "adjective",
                "def": "inactive; lazy; stagnant",
                "eg1": "After a long sleep, he still felt torpid.",
                "eg2": "It is a good day to go fishing as the sea is torpid."
            },
            {
                "word": "Deference",
                "key": 24,
                "type": "noun",
                "def": "respect",
                "eg1": "Hr addressed her with the deference due to age.",
                "eg2": "The actress was accorded all the deference of a visiting celebrity."
            },
            {
                "word": "Tangible",
                "key": 25,
                "type": "adjective",
                "def": "If something is tangible, it is clear enough or definite enough to be easily seen, felt, or noticed",
                "eg1": "There should be some tangible evidence that the economy is starting to recover.",
                "eg2": "There is a tangible sense of progress."
            }
        ]
    },
    {
        "name": "Section 8",
        "key": "8",
        "length": 25,
        "words": [
            {
                "word": "Lampoon",
                "key": 1,
                "type": "verb /noun",
                "def": "publicly criticize by using ridicule, irony, or sarcasm(verb); a speech or text lampooning someone or something(noun)",
                "eg1": "The actor was lampooned(verb) by the press after his chats were leaked.",
                "eg2": "the magazine fired at God, Royalty, and politicians, using cartoons and lampoons(noun)."
            },
            {
                "word": "Polemical",
                "key": 2,
                "type": "adjective",
                "def": "causing debate or argument",
                "eg1": "He was bombarded by the press for his polemical essay.",
                "eg2": "The letter was probably written for polemical impact. It is scarcely credible."
            },
            {
                "word": "Plagiarism",
                "key": 3,
                "type": "noun",
                "def": "the practice of taking someone else's work or ideas and passing them off as one's own",
                "eg1": "The medical journal accused the professor of plagiarism"
            },
            {
                "word": "Decoy",
                "key": 4,
                "type": "noun/verb",
                "def": "lure; trap; trick",
                "eg1": "To hunt the crocodile, the hunter set up a decoy(noun) duck.",
                "eg2": "They would try to decoy(verb) the enemy towards the hidden group."
            },
            {
                "word": "Abrogate",
                "key": 5,
                "type": "verb",
                "def": "cancel; deny; repeal",
                "eg1": "The next prime minister could abrogate the treaty.",
                "eg2": "Some parents completely abrogate responsibility for parenting their children."
            },
            {
                "word": "Obscure",
                "key": 6,
                "type": "adjective/verb",
                "def": "adjective/verb",
                "eg1": "his origins and parentage are obscure(adjective).",
                "eg2": "Grey clouds obscure the sun(verb)."
            },
            {
                "word": "Obsession",
                "key": 7,
                "type": "noun",
                "def": "a persistent preoccupation with an idea or feeling",
                "eg1": "Gambling became an obsession, and he eventually lost everything.",
                "eg2": "The media's obsession with celebrities makes news uninformative."
            },
            {
                "word": "Heed",
                "key": 8,
                "type": "verb/noun",
                "def": "listen to",
                "eg1": "He should have heeded(verb) the warnings before it was too late.",
                "eg2": "Although he heard, he paid no heed(noun)."
            },
            {
                "word": "Resplendent",
                "key": 9,
                "type": "adjective",
                "def": "shining; glowing; attractive and impressive",
                "eg1": "Everyone looked at her as she wore her resplendent sea-green dress.",
                "eg2": "The northern sky was resplendent in stars of unbelievable intensity."
            },
            {
                "word": "Adulation",
                "key": 10,
                "type": "noun",
                "def": "strong admiration; worship",
                "eg1": "The actor basked in the adulation of his fans.",
                "eg2": "The book was received with adulation by critics."
            },
            {
                "word": "Adroit",
                "key": 11,
                "type": "adjective",
                "def": "skilful / skillful",
                "eg1": "He was never caught because he was adroit at tax avoidance.",
                "eg2": "After a lot of practise, she became adroit at dealing with difficult questions."
            },
            {
                "word": "Bourgeois",
                "key": 12,
                "type": "adjective",
                "def": "characterstic of middle class",
                "eg1": "It's a bit bourgeois, isn't it, buying an outdated phone?"
            },
            {
                "word": "Poignant",
                "key": 13,
                "type": "adjective",
                "def": "deeply moving; strongly affecting the emotions",
                "eg1": "A poignant reminder of the passing of time.",
                "eg2": "The photograph awakens poignant memories of happier days."
            },
            {
                "word": "Blighted",
                "key": 14,
                "type": "verb",
                "def": "damaged; destroyed; ruined",
                "eg1": "The scandal blighted the careers of several leading politicians.",
                "eg2": "Long-term unemployment blights our society."
            },
            {
                "word": "Cynical",
                "key": 15,
                "type": "adjective",
                "def": "believing that people act only out of selfish motives",
                "eg1": "Because of his cynical outlook, he doesn't trust anyone.",
                "eg2": "A cynical manipulation of public opinion."
            },
            {
                "word": "Tenacious",
                "key": 16,
                "type": "adjective",
                "def": "stubborn; resolute; holding firm to a purpose",
                "eg1": "He is regarded as the most tenacious and persistent reporter.",
                "eg2": "She is very tenacious and will work hard and long to achieve objectives."
            },
            {
                "word": "Legion",
                "key": 17,
                "type": "noun/adjective",
                "def": "in large numbers",
                "eg1": "He was surrounded by legions(noun) of photographers and TV cameras.",
                "eg2": "Her fans are legion(adjective)."
            },
            {
                "word": "Decathlon",
                "key": 18,
                "type": "noun",
                "def": "an athletic competition with ten events",
                "eg1": "The decathlon is a very prestigious event at the Olympics.",
                "eg2": "The decathlon consists of ten different com petitions held over a two day span."
            },
            {
                "word": "Abrasive",
                "key": 19,
                "type": "adjective",
                "def": "showing little concern for the feelings of others; harsh.",
                "eg1": "Her abrasive and arrogant personal style won her few friends."
            },
            {
                "word": "Bigot",
                "key": 20,
                "type": "noun",
                "def": "Narrow-minded, prejudiced person",
                "eg1": "Because Helen is very close-minded, many people consider her to be a bigot.",
                "eg2": "My mother is a self-proclaimed bigot who believes all teenagers are bad."
            },
            {
                "word": "Eulogy",
                "key": 21,
                "type": "noun",
                "def": "praise; especially for a dead person",
                "eg1": "He delivered a heartfelt eulogy at his mother's funeral."
            },
            {
                "word": "Restorative",
                "key": 22,
                "type": "adjective/noun",
                "def": "having the ability to restore health, strength, or well-being(verb)",
                "eg1": "The restorative(adj) power of long walks is unparalleled.",
                "eg2": "They serve herbal restoratives(noun)."
            },
            {
                "word": "Objective",
                "key": 23,
                "type": "adjective",
                "def": "unbiased; not subjective",
                "eg1": "Historians try to be objective and impartial so as to be precise in their research.",
                "eg2": "You should try to be objective if you want to spend your money wisely."
            },
            {
                "word": "Enigma",
                "key": 24,
                "type": "noun",
                "def": "mystery; difficult to understand",
                "eg1": "Mathematics is an enigma to many.",
                "eg2": "Iran remains an enigma for the outside world."
            },
            {
                "word": "Reprehensible",
                "key": 25,
                "type": "adjective",
                "def": "shameful; very bad",
                "eg1": "She said the violence by anti-government protestors was reprehensible.",
                "eg2": "...behaving in the most reprehensible manner."
            }
        ]
    },
    {
        "name": "Section 9",
        "key": "9",
        "length": 25,
        "words": [
            {
                "word": "Ogle",
                "key": 1,
                "type": "verb",
                "def": "stare at; observe in an obvious manner",
                "eg1": "He ogled at all the attractive girls in the office.",
                "eg2": "I saw you ogling the woman in the red dress!"
            },
            {
                "word": "Caucus",
                "key": 2,
                "type": "noun",
                "def": "type of private political meeting",
                "eg1": "The states will hold precinct caucuses on Tuesday to choose delegates."
            },
            {
                "word": "Candid",
                "key": 3,
                "type": "adjective",
                "def": "frank; honest",
                "eg1": "His responses were remarkably candid.",
                "eg2": "To be perfectly candid, he said that he did not even like her."
            },
            {
                "word": "Livid",
                "key": 4,
                "type": "verb",
                "def": "very angry",
                "eg1": "The warden was livid that 4 prisoners had escaped.",
                "eg2": "She was absolutely livid that he had lied."
            },
            {
                "word": "Deplore",
                "key": 5,
                "type": "verb",
                "def": "1) express strong disapproval of; 2) regret strongly.",
                "eg1": "Like everyone else, I deplore and condemn this killing.",
                "eg2": "We deplore the use of violence against innocent people."
            },
            {
                "word": "Cacophony",
                "key": 6,
                "type": "noun",
                "def": "discordant loud noises",
                "eg1": "A cacophony of deafening alarm bells.",
                "eg2": "A cacophony of voices in a dozen languages filled the train station."
            },
            {
                "word": "Predicament",
                "key": 7,
                "type": "noun",
                "def": "dilemma; difficult situation",
                "eg1": "Te club's financial predicament came as a shock to many.",
                "eg2": "Your refusal puts me in an awkward predicament."
            },
            {
                "word": "Transgress",
                "key": 8,
                "type": "verb",
                "def": "go beyond the limits of (what is morally, socially, or legally acceptable)",
                "eg1": "They have transgressed the treaty of peace.",
                "eg2": "The terms of the treaty were transgressed almost immediately."
            },
            {
                "word": "Caustic",
                "key": 9,
                "type": "adjective/noun",
                "def": "sarcastic in a scathing and bitter way(adjective); burning(noun)",
                "eg1": "The players were making caustic comments about the refereeing.",
                "eg2": "Mustard gas is a caustic(noun) substance."
            },
            {
                "word": "Ubiquitous",
                "key": 10,
                "type": "adjective",
                "def": "found everywhere; omnipresent",
                "eg1": "The element 'Carbon' is ubiquitous in all life forms.",
                "eg2": "Coffee shops are ubiquitous these days."
            },
            {
                "word": "Sagacious",
                "key": 11,
                "type": "adjective",
                "def": "having or showing keen mental discernment and good judgement",
                "eg1": "They were sagacious enough to avoid any outright confrontation.",
                "eg2": "A sagacious businessman seldom fails in his business."
            },
            {
                "word": "Ignominious",
                "key": 12,
                "type": "adjective",
                "def": "shameful",
                "eg1": "The marriage was considered especially ignominious since she was of royal descent.",
                "eg2": "They will wipe out the shame of their ignominious defeat if they win the next game."
            },
            {
                "word": "Truant",
                "key": 13,
                "type": "noun/adjective",
                "def": "shirker; someone absent without permission",
                "eg1": "Nick was truant seven days this month.",
                "eg2": "She often played truant and wrote her own sick notes."
            },
            {
                "word": "Transcribe",
                "key": 14,
                "type": "verb",
                "def": "put (thoughts, speech, or data) into written or printed form",
                "eg1": "All the interviews were taped and transcribed.",
                "eg2": "Clerks transcribe everything that is said in court."
            },
            {
                "word": "Robust",
                "key": 15,
                "type": "adjective",
                "def": "1) physically strong; 2) marked by richness and fullness of flavor",
                "eg1": "His robust strength made him survive the zombie apocalypse.",
                "eg2": "This dish is a robust mixture of fish, onions and tomatoes."
            },
            {
                "word": "Unalloyed",
                "key": 16,
                "type": "adjective",
                "def": "free from mixture; complete",
                "eg1": "He jumped with unalloyed delight when he heard the news.",
                "eg2": "We had the perfect holiday - two weeks of unalloyed bliss."
            },
            {
                "word": "Magnanimous",
                "key": 17,
                "type": "adjective",
                "def": "generous; big-hearted",
                "eg1": "Their manager was magnanimous in victory, and praised the losing team.",
                "eg2": "Donating this big a sum was a magnanimous gesture on their part."
            },
            {
                "word": "Libertarian",
                "key": 18,
                "type": "noun/adjective",
                "def": "someone who opposes tyranny",
                "eg1": "No true libertarian(noun) would ever support a culture where citizens must show their papers to travel.",
                "eg2": "He holds libertarian(adjective) views on most social issues."
            },
            {
                "word": "Tyro",
                "key": 19,
                "type": "noun",
                "def": "novice; beginner",
                "eg1": "I am a tyro in English.",
                "eg2": "She is a tyro in the art of writing poetry."
            },
            {
                "word": "Ameliorate",
                "key": 20,
                "type": "verb",
                "def": "make better",
                "eg1": "The reform did much to ameliorate living standards.",
                "eg2": "Steps have been taken to ameliorate the situation."
            },
            {
                "word": "Ambivalence",
                "key": 21,
                "type": "noun",
                "def": "lack of clarity; wavering; being undecided",
                "eg1": "The law's ambivalence about the importance of a victim's identity.",
                "eg2": "There was ambivalence among church members about women becoming priests."
            },
            {
                "word": "Ostentatious",
                "key": 22,
                "type": "adjective",
                "def": "showy",
                "eg1": "Your clothes are looking a little too ostentatious; I told you to wear simple clothes.",
                "eg2": "I want a house that can meet my needs. I don't like any unneccesarily ostentatious bungalows."
            },
            {
                "word": "Potent",
                "key": 23,
                "type": "adjective",
                "def": "powerful; compelling; strong",
                "eg1": "Cocaine is a very potent drug.",
                "eg2": "Thrones were potent symbols of authority back in the day."
            },
            {
                "word": "Cajole",
                "key": 24,
                "type": "verb",
                "def": "persuade someone to do something by means of flattery",
                "eg1": "He really knows how to cajole people into doing what he wants.",
                "eg2": "Aid workers do their best to cajole rich countries into helping."
            },
            {
                "word": "Overwrought",
                "key": 25,
                "type": "adjective",
                "def": "worked up; in an emotional state",
                "eg1": "She was in a very overwrought state after the accident.",
                "eg2": "She was so tired and overwrought that she burst into tears."
            }
        ]
    },
    {
        "name": "Section 10",
        "key": "10",
        "length": 25,
        "words": [
            {
                "word": "Derogatory",
                "key": 1,
                "type": "adjective",
                "def": "showing a critical or disrespectful attitude.",
                "eg1": "She tells me I'm fat and is always making derogatory remarks.",
                "eg2": "The word 'pig' is a derogatory term for policeman."
            },
            {
                "word": "Precipitous",
                "key": 2,
                "type": "adjective",
                "def": "1) dangerously high or steep; 2) done in a hurry",
                "eg1": "The track skirted a precipitous drop.",
                "eg2": "I do not appreciate this precipitous intervention."
            },
            {
                "word": "Impecunious",
                "key": 3,
                "type": "adjective",
                "def": "having little or no money",
                "eg1": "He came from a impecunious but respectable family.",
                "eg2": "For an impecunious woman of twenty-nine, she seems very happy."
            },
            {
                "word": "Impious",
                "key": 4,
                "type": "adjective",
                "def": "1) showing a lack of respect for God or religion; 2)wicked",
                "eg1": "The emperor's impious attacks on the Church annoyed the devotees.",
                "eg2": "This part of town is filled with impious villains."
            },
            {
                "word": "Capitulate",
                "key": 5,
                "type": "verb",
                "def": "cease to resist an opponent or an unwelcome demand",
                "eg1": "Eventually, the patriots had to capitulate to the enemy forces.",
                "eg2": "We will never capitulate to pressure from outside."
            },
            {
                "word": "Palisade",
                "key": 6,
                "type": "noun/verb",
                "def": "fence made of posts",
                "eg1": "The castle's defenses consisted mainly of wooden palisades(noun).",
                "eg2": "He palisaded(verb) his garden, thus keeping people from picking flowers."
            },
            {
                "word": "Presentiment",
                "key": 7,
                "type": "noun",
                "def": "an intuitive feeling about the future",
                "eg1": "She has a presentiment of disaster.",
                "eg2": "I had a presentiment that he was going to betray my trust."
            },
            {
                "word": "Devoured",
                "key": 8,
                "type": "verb",
                "def": "1) eat greedily; 2) destroy completely",
                "eg1": "He devoured half of his burger in one bite.",
                "eg2": "The hungry flames devoured the old house."
            },
            {
                "word": "Liniment",
                "key": 9,
                "type": "noun",
                "def": "soothing lotion",
                "eg1": "She let him treat her ankle and put liniment on it."
            },
            {
                "word": "Preeminent",
                "key": 10,
                "type": "adjective",
                "def": "surpassing all others; very distinguished in some way",
                "eg1": "He is the world's pre-eminent expert on beetles.",
                "eg2": "His philosophical thinking can be regarded as preeminent."
            },
            {
                "word": "Levity",
                "key": 11,
                "type": "noun",
                "def": "flippancy; joking about serious matters",
                "eg1": "The professor brought some much-needed levity into his lecture on economic theory.",
                "eg2": "It was a moment of levity amid tough negotiations."
            },
            {
                "word": "Postulate",
                "key": 12,
                "type": "verb",
                "def": "hypothesize; propose",
                "eg1": "Even if we postulate that she had a motive for the murder, that still doesn't mean she did it.",
                "eg2": "His theory postulated a rotatory movement for hurricanes."
            },
            {
                "word": "Inclination",
                "key": 13,
                "type": "noun",
                "def": "tendency; a leaning toward",
                "eg1": "My natural inclination is to avoid crowded places.",
                "eg2": "She had neither the time nor the inclination to help them."
            },
            {
                "word": "Amelioration",
                "key": 14,
                "type": "noun",
                "def": "improvement",
                "eg1": "A change in diet led to amelioration in his health.",
                "eg2": "The amelioration of working conditions made all the employees happy."
            },
            {
                "word": "Extradite",
                "key": 15,
                "type": "verb",
                "def": "deport from one country back to the home country",
                "eg1": "The British government attempted to extradite the suspects from Belgium.",
                "eg2": "The court refused to extradite political refugees."
            },
            {
                "word": "Opulent",
                "key": 16,
                "type": "adjective",
                "def": "wealthy; rich; magnificent",
                "eg1": "The more opulent tenants all live in the penthouses.",
                "eg2": "Oh! The opulent comfort of a limousine."
            },
            {
                "word": "Oust",
                "key": 17,
                "type": "verb",
                "def": "push out of a position",
                "eg1": "The dictators were ousted from power."
            },
            {
                "word": "Ruse",
                "key": 18,
                "type": "noun",
                "def": "trick; stratagem",
                "eg1": "She tried to think of a ruse to get Paul's money.",
                "eg2": "Oh what a clever ruse! You fooled me."
            },
            {
                "word": "Immutable",
                "key": 19,
                "type": "adjective",
                "def": "unchanging; permanent",
                "eg1": "This decision should not be seen as immutable.",
                "eg2": "Some people regard grammar as an immutable set of rules."
            },
            {
                "word": "Sage",
                "key": 20,
                "type": "noun/adjective",
                "def": "a wise person(noun; having wisdom(adjective)",
                "eg1": "The sage will help you sort out your common problems.",
                "eg2": "He was famous for his sage(adjective) advice to younger painters."
            },
            {
                "word": "Hypocritical",
                "key": 21,
                "type": "adjective",
                "def": "insincere",
                "eg1": "It's hypocritical to say one thing and do another.",
                "eg2": "It would be hypocritical of me to have a church wedding when I don't believe in God."
            },
            {
                "word": "Deplete",
                "key": 22,
                "type": "verb",
                "def": "use up; lessen",
                "eg1": "His health depleted very quickly after he got cancer.",
                "eg2": "Reservoirs have been depleted by years of drought."
            },
            {
                "word": "Scale",
                "key": 23,
                "type": "noun",
                "def": "1) level; 2) relative magnitude; 3)",
                "eg1": "On a global scale, 77% of energy is created from fossil fuels.",
                "eg2": "We had underestimated the scale of the problem."
            },
            {
                "word": "Sanctimonious",
                "key": 24,
                "type": "adjective",
                "def": "hypocritically superior",
                "eg1": "What happened to all the sanctimonious talk about putting his family first?",
                "eg2": "Many sanctimonious speeches were made about the need for honesty in government."
            },
            {
                "word": "Savant",
                "key": 25,
                "type": "noun",
                "def": "person with knowledge",
                "eg1": "He is considered a savant in the field of molecular biology."
            }
        ]
    },
    {
        "name": "Section 11",
        "key": "11",
        "length": 25,
        "words": [
            {
                "word": "Lobbyist",
                "key": 1,
                "type": "noun",
                "def": "person who tries to persuade someone to support a particular cause",
                "eg1": "One lobbyist billed the environmental group $20,000 for nine-months' work.",
                "eg2": "Like every really smart lobbyist, Boggs knows the importance of keeping a low profile."
            },
            {
                "word": "Precarious",
                "key": 2,
                "type": "adjective",
                "def": "unstable; risky",
                "eg1": "Do not climb up that precarious flight of stairs.",
                "eg2": "He made a precarious living as a painter."
            },
            {
                "word": "Desecrate",
                "key": 3,
                "type": "verb",
                "def": "to damage or pollute(a sacred place); spoil",
                "eg1": "More than 300 graves were desecrated.",
                "eg2": "It's a crime to desecrate the country's flag."
            },
            {
                "word": "Ruminate",
                "key": 4,
                "type": "verb",
                "def": "think deeply about something",
                "eg1": "We sat ruminating on the nature of existence.",
                "eg2": "She spent all day in bed, ruminating on her past."
            },
            {
                "word": "Saccharin",
                "key": 5,
                "type": "noun",
                "def": "falsely sweet",
                "eg1": "We use saccharin in substitution for sugar."
            },
            {
                "word": "Deleterious",
                "key": 6,
                "type": "adjective",
                "def": "harmful",
                "eg1": "Divorce is assumed to have deleterious effects on the children.",
                "eg2": "These drugs have a proven deleterious effect on the nervous system."
            },
            {
                "word": "Castigate",
                "key": 7,
                "type": "verb",
                "def": "scold strongly; reprimanded",
                "eg1": "He was castigated for setting a bad example.",
                "eg2": "He was castigated for his carelessness in doing homework."
            },
            {
                "word": "Ignominy",
                "key": 8,
                "type": "noun",
                "def": "shame",
                "eg1": "The ignominy of being imprisoned.",
                "eg2": "He feared the ignominy of being exposed as a spy."
            },
            {
                "word": "Lummox",
                "key": 9,
                "type": "noun",
                "def": "clumsy person",
                "eg1": "Be careful, you lummox, you just stamped on my foot!",
                "eg2": "It is said that the lummox had loused up their company's whole Business."
            },
            {
                "word": "Tractable",
                "key": 10,
                "type": "adjective",
                "def": "easily managed (controlled or taught or molded)",
                "eg1": "This approach helps to make the issues more tractable.",
                "eg2": "Gold and silver are tractable metals."
            },
            {
                "word": "Burgeon",
                "key": 11,
                "type": "verb",
                "def": "grow; flourish; put forth new shoots",
                "eg1": "The city has burgeoned out into the surrounding suburbs.",
                "eg2": "Our company's business is burgeoning now."
            },
            {
                "word": "Overt",
                "key": 12,
                "type": "adjective",
                "def": "obvious; not hidden",
                "eg1": "He shows no overt signs of his unhappiness.",
                "eg2": "er overt sexuality shocked cinema audiences."
            },
            {
                "word": "Exasperated",
                "key": 13,
                "type": "adjective",
                "def": "frustrated; annoyed",
                "eg1": "He let out a sign of exasperation.",
                "eg2": "Stop that noise,' he cried out in exasperation."
            },
            {
                "word": "Exculpate",
                "key": 14,
                "type": "verb",
                "def": "free someone from blame; pardon; acquit",
                "eg1": "The new evidence exculpated the mayor.",
                "eg2": "He has been exculpated from the charge."
            },
            {
                "word": "Magnate",
                "key": 15,
                "type": "noun",
                "def": "powerful businessman",
                "eg1": "1. My grandfather was considered a very influential and wealthy oil magnate.",
                "eg2": "1. Because he was a magnate in the fashion industry, he was always in need of reliable assistants."
            },
            {
                "word": "Lithe",
                "key": 16,
                "type": "adjective",
                "def": "flexible; supple(especially for a body)",
                "eg1": "She lay gazing up at his tall, lithe figure.",
                "eg2": "He had the lithe, athletic body of a ballet dancer."
            },
            {
                "word": "Onerous",
                "key": 17,
                "type": "adjective",
                "def": "burdensome; hard to undertake",
                "eg1": "He found his duties to be increasingly onerous.",
                "eg2": "This is the most onerous task I have ever undertaken."
            },
            {
                "word": "Impromptu",
                "key": 18,
                "type": "adjective/adverb",
                "def": "unrehearsed; spontaneous",
                "eg1": "The minister organised an impromptu(adjective) press conference.",
                "eg2": "When asked to give a speech, he spoke impromptu(adverb)."
            },
            {
                "word": "Carping",
                "key": 19,
                "type": "adjective",
                "def": "constant criticism; quibbling over insignificant details.",
                "eg1": "She has silenced the carping critics with a successful debut tour.",
                "eg2": "His grandfather is always carping about the young generation."
            },
            {
                "word": "Analgesic",
                "key": 20,
                "type": "adjective/noun",
                "def": "medicine to combat pain",
                "eg1": "Aspirin is a popular analgesic.",
                "eg2": "This cream contains a mild analgesic to soothe stings and bites."
            },
            {
                "word": "Ordain",
                "key": 21,
                "type": "verb",
                "def": "1) order officially; 2) make (someone) a priest or minister",
                "eg1": "Equal punishment was ordained for the two crimes.",
                "eg2": "He was ordained a minister before entering Parliament."
            },
            {
                "word": "Exposition",
                "key": 22,
                "type": "noun",
                "def": "1) clear explanation; 2) a large public exhibition of art or trade goods.",
                "eg1": "For the sake of exposition the duty will be analysed here in four parts.",
                "eg2": "His pictures were shown at the Paris exposition of 1878."
            },
            {
                "word": "Precedent",
                "key": 23,
                "type": "noun/adjective",
                "def": "a previous occurrence used as a guide",
                "eg1": "There are substantial precedents(noun) for using interactive media in training.",
                "eg2": "This case is similar to a precedent(adjective) case."
            },
            {
                "word": "Orthodox",
                "key": 24,
                "type": "adjective",
                "def": "conventional",
                "eg1": "He challenged the orthodox views on religion.",
                "eg2": "She is a cancer sufferer who has rejected orthodox medicine and shifted to ayurvedic."
            },
            {
                "word": "Olfactory",
                "key": 25,
                "type": "adjective",
                "def": "concerned with the sense of smell",
                "eg1": "The nose is an olfactory organ."
            }
        ]
    },
    {
        "name": "Section 12",
        "key": "12",
        "length": 25,
        "words": [
            {
                "word": "Ornate",
                "key": 1,
                "type": "adjective",
                "def": "1) highly decorated; 2) using unusual words and complex constructions",
                "eg1": "The ornate Town Hall was opened by Queen Victoria herself.",
                "eg2": "He uses peculiarly ornate and metaphorical language."
            },
            {
                "word": "Prerogative",
                "key": 2,
                "type": "noun",
                "def": "right or privilege",
                "eg1": "Education was once the prerogative of the elite.",
                "eg2": "A monarch has the prerogative of pardoning criminals."
            },
            {
                "word": "Denounce",
                "key": 3,
                "type": "verb",
                "def": "condemn; speak out against",
                "eg1": "Thee Assembly denounced the use of violence.",
                "eg2": "The president's political enemies were quick to denounce him."
            },
            {
                "word": "Evacuate",
                "key": 4,
                "type": "verb",
                "def": "vacate; empty; abandon",
                "eg1": "During the floods, several families were evacuated from their homes.",
                "eg2": "Army helicopters tried to evacuate the injured."
            },
            {
                "word": "Ambulatory",
                "key": 5,
                "type": "adjective",
                "def": "able to walk around (used of hospital patients)",
                "eg1": "We will be opening two new ambulatory care facilities for private patients in May."
            },
            {
                "word": "Deliberate",
                "key": 6,
                "type": "verb/adjective",
                "def": "to engage in careful consideration(verb); done consciously and intentionally(adjective)",
                "eg1": "she deliberated(verb) over the menu",
                "eg2": "He made a deliberate(adjective) attempt to provoke conflict."
            },
            {
                "word": "Luscious",
                "key": 7,
                "type": "adjective",
                "def": "having a pleasing rich effect; appealing to the senses",
                "eg1": "This is a luscious and fragrant dessert wine.",
                "eg2": "He fell for a luscious Spanish girl."
            },
            {
                "word": "Aloof",
                "key": 8,
                "type": "adjective",
                "def": "distant; detached; cold",
                "eg1": "He often keeps aloof when we chat together.",
                "eg2": "He stayed aloof from the bickering."
            },
            {
                "word": "Sanction",
                "key": 9,
                "type": "verb/noun",
                "def": "give approval to",
                "eg1": "The scheme was sanctioned(verb) by the court.",
                "eg2": "He appealed to the bishop for his sanction(noun)."
            },
            {
                "word": "Precocious",
                "key": 10,
                "type": "adjective",
                "def": "developing early",
                "eg1": "From an early age she displayed a precocious talent for music.",
                "eg2": "What a precocious child. He is reading Shakespeare at the age of ten!"
            },
            {
                "word": "Exonerate",
                "key": 11,
                "type": "verb",
                "def": "acquits; absolves; removes blame",
                "eg1": "Recently discovered evidence exonerated those involved.",
                "eg2": "He was exonerated from all responsibility for the accident."
            },
            {
                "word": "Exegesis",
                "key": 12,
                "type": "noun",
                "def": "scholarly explanation or interpretation",
                "eg1": "Upon exegesis, the jury will conclude that his argument is baseless.",
                "eg2": "For them, history is not the exegesis of an ideologically preordained text, but the reconstruction of the past."
            },
            {
                "word": "Tranquil",
                "key": 13,
                "type": "adjective",
                "def": "free from disturbance; peaceful (for an object or person)",
                "eg1": "She stared at the tranquil surface of the water.",
                "eg2": "There was a tranquil expression on his face."
            },
            {
                "word": "Despondent",
                "key": 14,
                "type": "adjective",
                "def": "having no hope; miserable",
                "eg1": "I feel despondent when my work is rejected.",
                "eg2": "He became/grew increasingly despondent when she failed to return his phone calls."
            },
            {
                "word": "Exemplify",
                "key": 15,
                "type": "verb",
                "def": "to serve as a good example; illustrate",
                "eg1": "He exemplified his point with an anecdote.",
                "eg2": "Gandhi exemplified the virtues of renunciation, asceticism and restraint."
            },
            {
                "word": "Turpitude",
                "key": 16,
                "type": "noun",
                "def": "depraved or wicked behaviour /character",
                "eg1": "He was considered unfit to hold office because of moral turpitude."
            },
            {
                "word": "Allay",
                "key": 17,
                "type": "verb",
                "def": "to lessen",
                "eg1": "The government is keen to allay the public's fears.",
                "eg2": "They are trying to allay public fears about the spread of the disease."
            },
            {
                "word": "Exorcism",
                "key": 18,
                "type": "noun",
                "def": "the expulsion of an evil spirit from a person or place",
                "eg1": "A priest performed an exorcism and afterwards the ghost disappeared."
            },
            {
                "word": "Ominous",
                "key": 19,
                "type": "adjective",
                "def": "threatening",
                "eg1": "There were ominous dark clouds gathering overhead.",
                "eg2": "The engine has been making an ominous sound for quite a few dyas."
            },
            {
                "word": "Ambiguity",
                "key": 20,
                "type": "noun",
                "def": "uncertainty; vagueness",
                "eg1": "There is a degree of ambiguity in this statement.",
                "eg2": "Incorrect choice of words leads to ambiguity for the reader."
            },
            {
                "word": "Burnish",
                "key": 21,
                "type": "verb",
                "def": "polish",
                "eg1": "To burnish copper, I would probably use a drill.",
                "eg2": "He missed no opportunity to burnish his image."
            },
            {
                "word": "Precept",
                "key": 22,
                "type": "noun",
                "def": "guiding principle",
                "eg1": "It is the legal precept of being innocent until proven guilty.",
                "eg2": "The Commissioner issued precepts requiring the companies to provide information."
            },
            {
                "word": "Lynch",
                "key": 23,
                "type": "verb",
                "def": "assassinate; kill without legal sanction",
                "eg1": "They were about to lynch the President when reinforcements from the army burst into the room and rescued him."
            },
            {
                "word": "Unctuous",
                "key": 24,
                "type": "adjective",
                "def": "oily; using excessive flattery",
                "eg1": "He made an unctuous effort to appear religious to the voters.",
                "eg2": "He made an unctuous appraisal of the musical talent shown by the boss's daughter"
            },
            {
                "word": "Potable",
                "key": 25,
                "type": "adjective",
                "def": "suitable for drinking",
                "eg1": "There is no supply of potable water available here.",
                "eg2": "The state has strict hygienic regulations for potable water."
            }
        ]
    },
    {
        "name": "Section 13",
        "key": "13",
        "length": 25,
        "words": [
            {
                "word": "Loquacious",
                "key": 1,
                "type": "adjective",
                "def": "talkative",
                "eg1": "The normally loquacious Mr. Smith had nothing to say.",
                "eg2": "Jack became loquacious on his favorite topic."
            },
            {
                "word": "Trepidation",
                "key": 2,
                "type": "noun",
                "def": "a feeling of fear or anxiety about something that may happen",
                "eg1": "As soon as the earthquake hit, the men set off in fear and trepidation.",
                "eg2": "We view future developments with some trepidation."
            },
            {
                "word": "Execrable",
                "key": 3,
                "type": "adjective",
                "def": "extremely bad or unpleasant",
                "eg1": "How can you eat such execrble food?",
                "eg2": "Oh my! Such execrbale handwriting."
            },
            {
                "word": "Amity",
                "key": 4,
                "type": "noun",
                "def": "a state of friendship and cordiality",
                "eg1": "He lives in amity with his neighbours.",
                "eg2": "The two groups had lived in perfect amity for many years before the recent troubles."
            },
            {
                "word": "Prescient",
                "key": 5,
                "type": "noun",
                "def": "having or showing knowledge of events before they take place",
                "eg1": "The prescient economist was one of the few to see the financial collapse coming.",
                "eg2": "He predicted their response with amazing prescience."
            },
            {
                "word": "Sacrosanct",
                "key": 6,
                "type": "adjective",
                "def": "regarded as too important or valuable to be interfered with",
                "eg1": "Freedom of the press is sacrosanct.",
                "eg2": "In India, the cow is a sacrosanct animal."
            },
            {
                "word": "Trinket",
                "key": 7,
                "type": "noun",
                "def": "something of little value(usually jewellery)",
                "eg1": "She always returns from vacation with a few souvenirs, even if they're only cheap trinkets.",
                "eg2": "His wife liked little trinkets for her dressing table."
            },
            {
                "word": "Delineation",
                "key": 8,
                "type": "noun",
                "def": "the action of describing or portraying something",
                "eg1": "The artist's exquisite delineation of costume and jewellery.",
                "eg2": "The X-Ray provided excellent delineation of the bone cracks."
            },
            {
                "word": "Salutary",
                "key": 9,
                "type": "adjective",
                "def": "something which teaches you a lesson; beneficial",
                "eg1": "The accident was a salutary reminder of the dangers of climbing.",
                "eg2": "This would have a salutary effect upon the health of the family."
            },
            {
                "word": "Omnipotent",
                "key": 10,
                "type": "adjective",
                "def": "all-powerful",
                "eg1": "He worked in fear of his omnipotent boss.",
                "eg2": "God is considered by many to be an omnipotent entity."
            },
            {
                "word": "Sanguinary",
                "key": 11,
                "type": "adjective",
                "def": "accompanied by bloodshed",
                "eg1": "A sanguinary encounter seemed imminent between the two mobs.",
                "eg2": "A revolution is violent, turbulent and sanguinary ."
            },
            {
                "word": "Desecrate",
                "key": 12,
                "type": "verb",
                "def": "to damage or pollute(a sacred place); spoil",
                "eg1": "More than 300 graves were desecrated.",
                "eg2": "It's a crime to desecrate the country's flag."
            },
            {
                "word": "Euphemism",
                "key": 13,
                "type": "noun",
                "def": "a polite phrase to cover something unpleasant",
                "eg1": "Senior citizen' is a euphemism for 'old person'.",
                "eg2": "User fees' is just a politician's euphemism for taxes."
            },
            {
                "word": "Amass",
                "key": 14,
                "type": "verb",
                "def": "gather or accumulate, usually over a period of time",
                "eg1": "He amassed a fortune estimated at close to a million pounds.",
                "eg2": "The soldiers were amassing from all parts of Spain."
            },
            {
                "word": "Alacrity",
                "key": 15,
                "type": "noun",
                "def": "eagerness; enthusiasm; quickness",
                "eg1": "She accepted the invitation with alacrity.",
                "eg2": "They fell on the sandwiches with alacrity."
            },
            {
                "word": "Ossify",
                "key": 16,
                "type": "verb",
                "def": "1. turn to bone; 2. become fixed and rigid",
                "eg1": "These cartilages may ossify if not nourished.",
                "eg2": "Her attitude towards the relationship has ossified."
            },
            {
                "word": "Destitution",
                "key": 17,
                "type": "noun",
                "def": "hardship; poverty; misery",
                "eg1": "During the recession, the family faced eviction and destitution."
            },
            {
                "word": "Catharsis",
                "key": 18,
                "type": "noun",
                "def": "the process of releasing strong or repressed emotions",
                "eg1": "Music is a means of catharsis for most people.",
                "eg2": "The process of catharsis can bring improvements in both physical and mental health."
            },
            {
                "word": "Depravity",
                "key": 19,
                "type": "adjective",
                "def": "moral corruption",
                "eg1": "Experiencing life does not mean depravity and indulgence in drugs.",
                "eg2": "People were shocked by the depravity of her actions."
            },
            {
                "word": "Extol",
                "key": 20,
                "type": "verb",
                "def": "praise",
                "eg1": "He extolled the virtues of the Indian people.",
                "eg2": "Doctors often extol the virtues of eating less fat."
            },
            {
                "word": "Salubrious",
                "key": 21,
                "type": "adjective",
                "def": "1) health-giving; 2) (of a place) pleasant",
                "eg1": "His salubrious habits are the main cause of his good health.",
                "eg2": "An over-priced flat in a none too salubrious area."
            },
            {
                "word": "Precipice",
                "key": 22,
                "type": "noun",
                "def": "steep slope",
                "eg1": "Our car swerved toward the edge of the precipice.",
                "eg2": "A loose rock tumbled over the precipice."
            },
            {
                "word": "Precinct",
                "key": 23,
                "type": "noun",
                "def": "the area within the boundaries of a particular place; a police station",
                "eg1": "The former MP still works in the precincts of the House.",
                "eg2": "The murder occurred just a block from the precinct."
            },
            {
                "word": "Lofty",
                "key": 24,
                "type": "adjective",
                "def": "elevated in height,spirit,character ; elevated in status",
                "eg1": "The lofty walls of the castle seemed impregnable.",
                "eg2": "He kicked out the less lofty customers of the bar."
            },
            {
                "word": "Lethargic",
                "key": 25,
                "type": "adjective",
                "def": "tired; without energy",
                "eg1": "After working the full day, I felt very lethargic.",
                "eg2": "The hot weather was making us all lethargic."
            }
        ]
    },
    {
        "name": "Section 14",
        "key": "14",
        "length": 25,
        "words": [
            {
                "word": "Palatable",
                "key": 1,
                "type": "adjective",
                "def": "1) pleasnat tasting(food); 2) acceptable/satisfactory(for an action)",
                "eg1": "This is a very palatable local red wine.",
                "eg2": "This is a method that makes increased taxation more palatable."
            },
            {
                "word": "Detrimental",
                "key": 2,
                "type": "adjective",
                "def": "harmful",
                "eg1": "Poor eating habits are detrimental to health.",
                "eg2": "These chemicals have a detrimental impact on the environment."
            },
            {
                "word": "Expedite",
                "key": 3,
                "type": "verb",
                "def": "make faster",
                "eg1": "The Prime Minister promised to expedite economic reforms.",
                "eg2": "The builders promised to expedite the repairs."
            },
            {
                "word": "Incarceration",
                "key": 4,
                "type": "noun",
                "def": "putting in prison",
                "eg1": "There have been angry protests about the civil leader's arrest and incarceration.",
                "eg2": "Her childhood was overshadowed by her mother's incarceration in a psychiatric hospital."
            },
            {
                "word": "Underscore",
                "key": 5,
                "type": "verb",
                "def": "emphasize",
                "eg1": "Studies underscore that dolphins are smarter than some humans.",
                "eg2": "She arrived early to underscore the importance of the occasion."
            },
            {
                "word": "Alleviate",
                "key": 6,
                "type": "verb",
                "def": "make less severe",
                "eg1": "he couldn't prevent her pain, only alleviate it.",
                "eg2": "The organization works to alleviate world hunger and disease."
            },
            {
                "word": "Alibi",
                "key": 7,
                "type": "noun",
                "def": "an excuse that shows someone was not at a crime scene",
                "eg1": "She has an alibi to prove she was elsehwere yesterday evening.",
                "eg2": "The false alibi threw the police off the scent."
            },
            {
                "word": "Desist",
                "key": 8,
                "type": "verb",
                "def": "stop; discontinue; cease",
                "eg1": "The mob pledged to desist from acts of violence.",
                "eg2": "The shopping centre agreed to desist from false advertising."
            },
            {
                "word": "Deteriorate",
                "key": 9,
                "type": "verb",
                "def": "worsen; decline",
                "eg1": "Relations between the countries have deteriorated sharply.",
                "eg2": "This health deteriorated after his encounter with cancer."
            },
            {
                "word": "Deride",
                "key": 10,
                "type": "verb",
                "def": "ridicule; make fun of; mock",
                "eg1": "The decision to cut down trees was derided by environmentalists.",
                "eg2": "Feel free to deride me for my mistakes."
            },
            {
                "word": "Euphony",
                "key": 11,
                "type": "noun",
                "def": "pleasant sounds",
                "eg1": "The poet put euphony before mere rhyming."
            },
            {
                "word": "Rotund",
                "key": 12,
                "type": "adjective",
                "def": "large and plump(for a person); full and rich(for sounds)",
                "eg1": "Her brother was slim where she was rotund.",
                "eg2": "Picasso's music is considered rotund by all."
            },
            {
                "word": "Capacious",
                "key": 13,
                "type": "adjective",
                "def": "spacious; large in capacity",
                "eg1": "She rummaged for her lipstick in her capacious handbag.",
                "eg2": "Swimming demands long leg bones, powerful muscles, capacious lungs."
            },
            {
                "word": "Scapegoat",
                "key": 14,
                "type": "noun",
                "def": "person on whom blame is placed for faults of others",
                "eg1": "He has been made a scapegoat for the company's failures.",
                "eg2": "The captain was made a scapegoat for the team's failure."
            },
            {
                "word": "Candor",
                "key": 15,
                "type": "noun",
                "def": "frankness; openness",
                "eg1": "Maintaining any relationship requires commitment and candor.",
                "eg2": "He is a man of refreshing candour."
            },
            {
                "word": "Predecessor",
                "key": 16,
                "type": "noun",
                "def": "one who came before",
                "eg1": "My predecessor worked in this job for twelve years.",
                "eg2": "This computer has a larger memory than its predecessor."
            },
            {
                "word": "Anarchy",
                "key": 17,
                "type": "noun",
                "def": "chaos; lack of government",
                "eg1": "The country was thrown into a state of anarchy.",
                "eg2": "There would be anarchy if we had no police."
            },
            {
                "word": "Undermined",
                "key": 18,
                "type": "verb",
                "def": "damaged; attacked",
                "eg1": "The flow of water had undermined the pillars supporting the bridge.",
                "eg2": "This action could undermine years of hard work."
            },
            {
                "word": "Omniscient",
                "key": 19,
                "type": "adjective",
                "def": "all-knowing",
                "eg1": "God is typically described by Western religions as omniscient.",
                "eg2": "Even the botanical garden's omniscient botanist couldn't explain the meaning of the flower's name."
            },
            {
                "word": "Tumult",
                "key": 20,
                "type": "noun",
                "def": "uproar; noise",
                "eg1": "A tumult of shouting and screaming suddenly broke out.",
                "eg2": "A tumult of feelings inside her fought for love and respect."
            },
            {
                "word": "Illuminate",
                "key": 21,
                "type": "verb",
                "def": "to light up or make clear",
                "eg1": "a flash of lightning illuminated the house",
                "eg2": "Placing the events of the 1930s in a broader historical context helps to illuminate their significance."
            },
            {
                "word": "Idiosyncrasy",
                "key": 22,
                "type": "noun",
                "def": "a personal peculiarity; something unique to an individual",
                "eg1": "Oe of his little idiosyncrasies was always preferring to be in the car first.",
                "eg2": "Eating no meat was Amy's idiosyncrasy."
            },
            {
                "word": "Expedient",
                "key": 23,
                "type": "adjective",
                "def": "convenient/practical (mostly immoral)",
                "eg1": "Either side could break the agreement if it were expedient to do so.",
                "eg2": "It is expedient that he should retire at once."
            },
            {
                "word": "Iconoclast",
                "key": 24,
                "type": "noun",
                "def": "person who opposes orthodoxy",
                "eg1": "He does not believe in idol worship. He is an iconoclast."
            },
            {
                "word": "Amorphous",
                "key": 25,
                "type": "adjective",
                "def": "lacking in structure; shape",
                "eg1": "I really can't understand his amorphous ideas.",
                "eg2": "An amorphous and leaderless committee headed the party."
            }
        ]
    },
    {
        "name": "Section 15",
        "key": "15",
        "length": 25,
        "words": [
            {
                "word": "Buttress",
                "key": 1,
                "type": "verb",
                "def": "strengthen; support",
                "eg1": "We buttressed the wall as it was showing signs of cracking and collapse.",
                "eg2": "Authority was buttressed by religious belief."
            },
            {
                "word": "Sanguine",
                "key": 2,
                "type": "adjective/noun",
                "def": "optimistic or positive, especially in an apparently bad or difficult situation",
                "eg1": "He is sanguine about prospects for the global economy.",
                "eg2": "They are less sanguine regarding world peace."
            },
            {
                "word": "Trivial",
                "key": 3,
                "type": "adjective",
                "def": "unimportant",
                "eg1": "It was outrageous that huge fines were imposed for trivial offences.",
                "eg2": "He could remember every trivial incident in great detail."
            },
            {
                "word": "Traverse",
                "key": 4,
                "type": "verb",
                "def": "to move across",
                "eg1": "It is difficult to traverse the shaky bridge.",
                "eg2": "We need to traverse the last century thoroughly."
            },
            {
                "word": "Callow",
                "key": 5,
                "type": "adjective",
                "def": "inexperienced and immature",
                "eg1": "This is clearly not a boast; it seems, rather, a shamed admission of petty, callow cruelty.",
                "eg2": "He was just a callow youth of sixteen when he arrived in Paris."
            },
            {
                "word": "Preamble",
                "key": 6,
                "type": "noun",
                "def": "introductory material",
                "eg1": "He launched into his statement without any preamble thus confusing everybody.",
                "eg2": "She gave him the bad news without preamble."
            },
            {
                "word": "Altruism",
                "key": 7,
                "type": "adjective",
                "def": "putting others first; being self-sacrificing",
                "eg1": "Mother Teresa was a very altruistic person."
            },
            {
                "word": "Lukewarm",
                "key": 8,
                "type": "adjective",
                "def": "1. unenthusiastic; 2. neither hot nor cold",
                "eg1": "The film received a lukewarm reception from critics.",
                "eg2": "She likes to drink lukewarm coffee."
            },
            {
                "word": "Trite",
                "key": 9,
                "type": "adjective",
                "def": "unoriginal; dull",
                "eg1": "The movie is teeming with obvious and trite ideas.",
                "eg2": "This point may now seem obvious and trite."
            },
            {
                "word": "Exacerbate",
                "key": 10,
                "type": "verb",
                "def": "make worse",
                "eg1": "Interfering now would only exacerbate the situation.",
                "eg2": "Cutting trees will only exacerbate climate change problems."
            },
            {
                "word": "Deter",
                "key": 11,
                "type": "verb",
                "def": "put off; prevent",
                "eg1": "Only a health problem would deter him from chasing his goals.",
                "eg2": "These measures are designed to deter an enemy attack."
            },
            {
                "word": "Preclude",
                "key": 12,
                "type": "verb",
                "def": "prevent or make impossible",
                "eg1": "The secret nature of his work precluded official recognition.",
                "eg2": "His difficulties preclude him from leading a normal life."
            },
            {
                "word": "Incantation",
                "key": 13,
                "type": "noun",
                "def": "chant; invocation; prayer",
                "eg1": "The priest recited an incantation to raise the dead.",
                "eg2": "Around the fire, tribal elders chanted incantations."
            },
            {
                "word": "Demur",
                "key": 14,
                "type": "verb/noun",
                "def": "the action of objecting to or hesitating over something; refusing",
                "eg1": "Normally she would have accepted the challenge, but she demurred(verb).",
                "eg2": "They accepted this ruling without demur(noun)."
            },
            {
                "word": "Sardonic",
                "key": 15,
                "type": "adjective",
                "def": "mocking or cynical",
                "eg1": "He raised a sardonic eyebrow as she sat down opposite him and started to eat.",
                "eg2": "The scar gave his face a mocking, sardonic cast except when he smiled."
            },
            {
                "word": "Transient",
                "key": 16,
                "type": "adjective/noun",
                "def": "short-lived; ephemeral",
                "eg1": "The transient rainfall of October.",
                "eg2": "Their happiness was to be sadly transient."
            },
            {
                "word": "Salacious",
                "key": 17,
                "type": "adjective",
                "def": "lecherous; erotic",
                "eg1": "Some papers seek out extra salacious background.",
                "eg2": "Under domestic law, salacious images of children aged less than 18 are considered child pornography."
            },
            {
                "word": "Exemplary",
                "key": 18,
                "type": "adjective",
                "def": "outstandingly good; setting a fine example",
                "eg1": "We thank the army for their exemplary service.",
                "eg2": "Her behaviour was exemplary."
            },
            {
                "word": "Ambiguous",
                "key": 19,
                "type": "adjective",
                "def": "unclear in meaning; can be interpreted in different ways",
                "eg1": "His reply to my question was somewhat ambiguous.",
                "eg2": "The government has been ambiguous on this issue."
            },
            {
                "word": "Illusory",
                "key": 20,
                "type": "adjective",
                "def": "deceptive; false; misleading",
                "eg1": "First impressions can often prove illusory.",
                "eg2": "She knew the safety of her room was illusory and the attackers could come in at any moment."
            },
            {
                "word": "Incipient",
                "key": 21,
                "type": "adjective",
                "def": "just beginning",
                "eg1": "He could feel incipient anger building up.",
                "eg2": "During the incipient phase, vaccinations were very slow."
            },
            {
                "word": "Ludicrous",
                "key": 22,
                "type": "adjective",
                "def": "ridiculous; silly",
                "eg1": "It's ludicrous that I have been fined!",
                "eg2": "He looked ludicrous in that suit!"
            },
            {
                "word": "Alchemy",
                "key": 23,
                "type": "noun",
                "def": "medieval chemistry; magical chemical process",
                "eg1": "She manages, by some extraordinary alchemy, to turn the most ordinary of ingredients into the most delicious of dishes.",
                "eg2": "Mercury was essential in the alchemy process to refine gold and silver."
            },
            {
                "word": "Exhaustive",
                "key": 24,
                "type": "adjective",
                "def": "complete and thorough",
                "eg1": "The guide outlines every bus route in exhaustive detail.",
                "eg2": "Let's do an exhaustive study regarding the subject."
            },
            {
                "word": "Caldron",
                "key": 25,
                "type": "noun",
                "def": "huge cooking pot(not literally)",
                "eg1": "The stadium was a seething cauldron of emotion.",
                "eg2": "Several crabs were thrown into a boiling cauldron."
            }
        ]
    },
    {
        "name": "Section 16",
        "key": "16",
        "length": 25,
        "words": [
            {
                "word": "Cantankerous",
                "key": 1,
                "type": "adjective",
                "def": "bad-tempered; quarrelsome",
                "eg1": "He can be very cantankerous when his patience runs out.",
                "eg2": null
            },
            {
                "word": "Onus",
                "key": 2,
                "type": "noun",
                "def": "an onerous or difficult concern.",
                "eg1": "The onus of proof lies with you.",
                "eg2": "The onus is on employers to follow health and safety laws."
            },
            {
                "word": "Cavalcade",
                "key": 3,
                "type": "noun",
                "def": "procession of vehicles",
                "eg1": "The royal cavalcade proceeded through the city."
            },
            {
                "word": "Immoderate",
                "key": 4,
                "type": "adjective",
                "def": "excessive; extreme",
                "eg1": "Stop with the immoderate drinking.",
                "eg2": "She contracted such an immoderate attachment to her scoured purse."
            },
            {
                "word": "Truncate",
                "key": 5,
                "type": "verb",
                "def": "cut short",
                "eg1": "He was a sensational player whose career was truncated by injuries.",
                "eg2": "Further discussion was truncated by the arrival of tea."
            },
            {
                "word": "Longevity",
                "key": 6,
                "type": "noun",
                "def": "long life",
                "eg1": "I wish you longevity and good health.",
                "eg2": "He prides himself on the longevity of the company."
            },
            {
                "word": "Amiable",
                "key": 7,
                "type": "adjective",
                "def": "friendly",
                "eg1": "The amiable young man greeted me enthusiastically.",
                "eg2": "The next door neighbours are very amiable people."
            },
            {
                "word": "Maelstrom",
                "key": 8,
                "type": "noun",
                "def": "a situation or state of confused movement or violent turmoil.",
                "eg1": "The train station was a maelstrom of crowds.",
                "eg2": "Inside, she was a maelstrom of churning emotions."
            },
            {
                "word": "Impoverished",
                "key": 9,
                "type": "adjective",
                "def": "destitute; poor",
                "eg1": "He was fired for his use of impoverished and debased language.",
                "eg2": "Heavy rain and excessive use have impoverished the soil."
            },
            {
                "word": "Palliative",
                "key": 10,
                "type": "adjective/noun",
                "def": "a remedy that improves but doesn't cure",
                "eg1": "Orthodox medicines tend to be palliative(adjective) rather than curative.",
                "eg2": "The doctor prescribed pallatives(noun) for her irreparable condition."
            },
            {
                "word": "Machinations",
                "key": 11,
                "type": "noun",
                "def": "plots and plans",
                "eg1": "After being caught running a machination against his political rival, the ruthless candidate lost the election.",
                "eg2": "I don't want to get involved in all his machinations."
            },
            {
                "word": "Expatriate",
                "key": 12,
                "type": "noun/verb",
                "def": "someone living away from his own country(noun); expel from a country(verb)",
                "eg1": "There are many American expatriates(noun) in London.",
                "eg2": "We expatriated(verb) the prisoners of war immediately after the end of the war."
            },
            {
                "word": "Pragmatic",
                "key": 13,
                "type": "adjective",
                "def": "practical",
                "eg1": "We need to adopt a more pragmatic approach than a blind one.",
                "eg2": "In business, the pragmatic approach to problems is often more successful than an idealistic one."
            },
            {
                "word": "Analogous",
                "key": 14,
                "type": "adjective",
                "def": "comparable",
                "eg1": "The company is in a position closely analogous to that of its main rival.",
                "eg2": "This proposal was analogous to the one we discussed at the last meeting."
            },
            {
                "word": "Exceptionable",
                "key": 15,
                "type": "adjective",
                "def": "open to objection; causing disapproval or offence",
                "eg1": "His drawings are almost the only exceptionable part of his work.",
                "eg2": "There are no exceptionable scenes in the play."
            },
            {
                "word": "Lucid",
                "key": 16,
                "type": "adjective",
                "def": "expressed clearly; easy to understand",
                "eg1": "You must write in a clear and lucid style.",
                "eg2": "She gave a clear and lucid account of her plans for the company's future."
            },
            {
                "word": "Cartographer",
                "key": 17,
                "type": "noun",
                "def": "person who makes maps",
                "eg1": "He is a cartographer in the Indian army."
            },
            {
                "word": "Sallow",
                "key": 18,
                "type": "adjective",
                "def": "(of a person's face or complexion) of an unhealthy yellowish colour",
                "eg1": "His skin was sallow and pitted."
            },
            {
                "word": "Incessant",
                "key": 19,
                "type": "adjective",
                "def": "without stopping",
                "eg1": "We have had incessant snowfall since yesterday afternoon.",
                "eg2": "The noise of bombs and guns was incessant."
            },
            {
                "word": "Deprecate",
                "key": 20,
                "type": "verb",
                "def": "criticize; denounce",
                "eg1": "I deprecate him constantly interfering with my life.",
                "eg2": "He deprecates the value of children's television."
            },
            {
                "word": "Opaque",
                "key": 21,
                "type": "adjetcive",
                "def": "not transparent; in content or thought",
                "eg1": "This liquid is opaque, unlike water, you can't see throught it.",
                "eg2": "The vocablury in his talk was opaque to me."
            },
            {
                "word": "Inadvertent",
                "key": 22,
                "type": "adjective",
                "def": "not intentional",
                "eg1": "An inadvertent system error occurred that resulted in an overpayment.",
                "eg2": "Dan's blunder was inadvertent and we forgave him."
            },
            {
                "word": "Impartial",
                "key": 23,
                "type": "adjective",
                "def": "unbiased; neutral",
                "eg1": "Any court trial must be fair and impartial.",
                "eg2": "We offer impartial advice on tax and insurance."
            },
            {
                "word": "Pragmatist",
                "key": 24,
                "type": "noun",
                "def": "practical person; one who is concerned with usefulness",
                "eg1": "Our manager is a pragmatist who likes thorough research."
            },
            {
                "word": "Byline",
                "key": 25,
                "type": "noun",
                "def": "the line that tells you who wrote an article",
                "eg1": "She is a financial journalist with her own by-line."
            }
        ]
    },
    {
        "name": "Section 17",
        "key": "17",
        "length": 25,
        "words": [
            {
                "word": "Anecdote",
                "key": 1,
                "type": "noun",
                "def": "a brief amusing story",
                "eg1": "He told funny anecdotes about his travels.",
                "eg2": null
            },
            {
                "word": "Falter",
                "key": 2,
                "type": "verb",
                "def": "hesitate(for speech or movement)",
                "eg1": "He faltered and finally stopped mid-stride.",
                "eg2": "\"S-S-Sumit?\", Chinmay faltered."
            },
            {
                "word": "Pretentious",
                "key": 3,
                "type": "adjective",
                "def": "attempting to impress by affecting greater importance than is actually possessed",
                "eg1": "His response was full of pretentious nonsense.",
                "eg2": "He spouts a load of pretentious nonsense and people are stupid enough to believe him!"
            },
            {
                "word": "Charlatan",
                "key": 4,
                "type": "noun",
                "def": "a person falsely claiming to have a special knowledge or skill",
                "eg1": "He knows nothing about medicine-he's a complete charlatan.",
                "eg2": "He was exposed as a charlatan after they caught him."
            },
            {
                "word": "Didactic",
                "key": 5,
                "type": "adjective",
                "def": "intended to teach; instructive",
                "eg1": "He wrote a didactic novel that set out to expose social injustice.",
                "eg2": "I don't like her didactic way of explaining everything."
            },
            {
                "word": "Proclivity",
                "key": 6,
                "type": "noun",
                "def": "tendency towards",
                "eg1": "The child showed proclivity towards aggression.",
                "eg2": "1. I prefer to work with people that have a proclivity to be efficient and reliable."
            },
            {
                "word": "Antediluvian",
                "key": 7,
                "type": "adjective",
                "def": "outdated; prehistoric; very old-fashioned",
                "eg1": "She studies gigantic bones of antediluvian animals.",
                "eg2": "My mother has some hopelessly antediluvian ideas about the role of women."
            },
            {
                "word": "Prodigious",
                "key": 8,
                "type": "adjective",
                "def": "remarkably or impressively great in extent, size, or degree",
                "eg1": "The car consumed a prodigious amount of fuel.",
                "eg2": "What a prodigious opportunity you have missed!"
            },
            {
                "word": "Paragon",
                "key": 9,
                "type": "noun",
                "def": "a perfect example",
                "eg1": "He was considered to be a paragon of good etiquettes.",
                "eg2": "Our cook is a paragon of amazing baking skills!"
            },
            {
                "word": "Malingerer",
                "key": 10,
                "type": "noun",
                "def": "person who deliberately tries to avoid work",
                "eg1": "I'm sure my manager thinks I'm a malingerer."
            },
            {
                "word": "Indigenous",
                "key": 11,
                "type": "adjective",
                "def": "native to a particular area",
                "eg1": "These berries are indigenous to India.",
                "eg2": "Kangaroos are indigenous to Australia."
            },
            {
                "word": "Chastise",
                "key": 12,
                "type": "verb",
                "def": "punishe; reprimand",
                "eg1": "He chastised his colleagues for their laziness.",
                "eg2": "The father chastised his son for his misconduct."
            },
            {
                "word": "Fatuous",
                "key": 13,
                "type": "adjective",
                "def": "silly; foolish",
                "eg1": "Stop passing fatuous comments!",
                "eg2": "The Chef was left speechless by this fatuous remark."
            },
            {
                "word": "Unfetter",
                "key": 14,
                "type": "verb",
                "def": "release from a restraining or inhibiting force; set free",
                "eg1": "Unfettered by the bounds of reality, the artist's imagination flourished.",
                "eg2": "He also placed great emphasis on effective and unfettered communication."
            },
            {
                "word": "Maladroit",
                "key": 15,
                "type": "adjective",
                "def": "inefficient or inept; clumsy",
                "eg1": "Both parties are unhappy about the maladroit handling of the whole affair.",
                "eg2": "She can be a little maladroit in social situations."
            },
            {
                "word": "Serendipity",
                "key": 16,
                "type": "noun",
                "def": "fortunate coincidence; unsought discovery",
                "eg1": "There is a real element of serendipity in archaeology."
            },
            {
                "word": "Malefactor",
                "key": 17,
                "type": "noun",
                "def": "a wrong-doer",
                "eg1": "Malefactors will be pursued and punished.",
                "eg2": "Shortly after the crime, the malefactor was caught and turned over to the police."
            },
            {
                "word": "Paradox",
                "key": 18,
                "type": "noun",
                "def": "apparently contradictory statement",
                "eg1": "It's a curious paradox that drinking a lot of water can often make you feel thirsty.",
                "eg2": "It\u2019s a strange paradox that people who say you shouldn\u2019t criticize others criticize them as soon as they disagree with them."
            },
            {
                "word": "Paramount",
                "key": 19,
                "type": "adjective",
                "def": "of supreme importance; having supreme power",
                "eg1": "The upbringing of the child is of paramount importance.",
                "eg2": "The village had a paramount chief."
            },
            {
                "word": "Malediction",
                "key": 20,
                "type": "noun",
                "def": "a magical phrase uttered with the intention of bringing about evil; a curse",
                "eg1": "With her dying breath, the witch pronounced a malediction on the people who were burning her."
            },
            {
                "word": "Extrapolate",
                "key": 21,
                "type": "verb",
                "def": "extend; predict on the basis of known data",
                "eg1": "They use an intricate formula to extrapolate how many matches their team would win.",
                "eg2": "It is not always possible to extrapolate future developments from current trends."
            },
            {
                "word": "Dexterous",
                "key": 22,
                "type": "adjective",
                "def": "skilful with hands",
                "eg1": "She's very dexterous with the knitting needles.",
                "eg2": "As people grow older they generally become less dexterous."
            },
            {
                "word": "Digress",
                "key": 23,
                "type": "verb",
                "def": "wander off the subject",
                "eg1": "I have digressed a little from my original plan",
                "eg2": "The old professor used to digress from his subject for a moment to tell his students a funny story."
            },
            {
                "word": "Malleable",
                "key": 24,
                "type": "adjective",
                "def": "easily influenced; pliable",
                "eg1": "They are as malleable and easily led as sheep.",
                "eg2": "He had an actor's typically malleable features."
            },
            {
                "word": "Fallacious",
                "key": 25,
                "type": "adjective",
                "def": "based on a mistaken belief.",
                "eg1": "His argument is based on fallacious reasoning.",
                "eg2": "Such a bill would be entirely fallacious."
            }
        ]
    },
    {
        "name": "Section 18",
        "key": "18",
        "length": 25,
        "words": [
            {
                "word": "Prevaricate",
                "key": 1,
                "type": "verb",
                "def": "speak deliberately ambiguous or unclear in order to mislead",
                "eg1": "He seemed to prevaricate when journalists asked pointed questions.",
                "eg2": "Tell us exactly what happened and do not prevaricate."
            },
            {
                "word": "Serrated",
                "key": 2,
                "type": "adjective",
                "def": "jagged; saw-like",
                "eg1": "Bread knives should have a serrated edge."
            },
            {
                "word": "Cerebral",
                "key": 3,
                "type": "adjective",
                "def": "involving intelligence rather than emotions or instinct",
                "eg1": "Her novel is cerebral, yet also scary and funny.",
                "eg2": "His poetry is very cerebral."
            },
            {
                "word": "Inconspicuous",
                "key": 4,
                "type": "adjective",
                "def": "not easily seen; subtle; not noticeable",
                "eg1": "This type of bird is very inconspicuous because of its dull feathers.an inconspicuous red-brick building",
                "eg2": "She stood by the wall, trying to look inconspicuous."
            },
            {
                "word": "Marred",
                "key": 5,
                "type": "verb",
                "def": "damaged; spoiled",
                "eg1": "Violence marred a number of Christmas celebrations.",
                "eg2": "Sadly, the text is marred by careless errors."
            },
            {
                "word": "Sequester",
                "key": 6,
                "type": "verb",
                "def": "isolate; set apart",
                "eg1": "The jury were sequestered during the trial.",
                "eg2": "Their property was sequestered by the government."
            },
            {
                "word": "Panacea",
                "key": 7,
                "type": "noun",
                "def": "a solution or remedy for all difficulties or diseases",
                "eg1": "Meditation is the universal panacea for mental peace.",
                "eg2": "Technology is not a panacea for all our problems."
            },
            {
                "word": "Annex",
                "key": 8,
                "type": "verb",
                "def": "1) add as an extra part, especially to a document; 2)take possession of; seize; capture",
                "eg1": "The first ten amendments were annexed to the Constitution in 1791.",
                "eg2": "The left bank of the Rhine was annexed by France in 1797."
            },
            {
                "word": "Dichotomy",
                "key": 9,
                "type": "noun",
                "def": "a division or contrast between two things that are or are represented as being opposed or entirely different.",
                "eg1": "There exists a rigid dichotomy between science and religion.",
                "eg2": "There is often a dichotomy between what politicians say and what they do."
            },
            {
                "word": "Diffident",
                "key": 10,
                "type": "adjective",
                "def": "modest or shy because of a lack of self-confidence",
                "eg1": "You shouldn't be so diffident about your achievements - you've done really well!",
                "eg2": "We are diffident about making arrangements which appear to benefit us."
            },
            {
                "word": "Celerity",
                "key": 11,
                "type": "noun",
                "def": "swiftness of movement.",
                "eg1": "The slave will be punished if his celerity is not fast enough for his master.",
                "eg2": "When the commanding officer tells his soldiers to move, the men and women move with purposeful celerity."
            },
            {
                "word": "Uproarious",
                "key": 12,
                "type": "adjective",
                "def": "hilarious; hysterical; very funny",
                "eg1": "His uproarious humour made everyone laugh.",
                "eg2": "The noise of talk and laughter was uproarious."
            },
            {
                "word": "Presumptuous",
                "key": 13,
                "type": "adjective",
                "def": "failing to observe the limits of what is permitted or appropriate",
                "eg1": "I hope I won't be considered presumptuous if I offer some advice",
                "eg2": "He was presumptuous in making the announcement before the decision had been approved."
            },
            {
                "word": "Diligent",
                "key": 14,
                "type": "adjective",
                "def": "hard-working",
                "eg1": "After diligent searching, he found the right job.",
                "eg2": "He made a diligent attempt to learn Russian."
            },
            {
                "word": "Animosity",
                "key": 15,
                "type": "adjective",
                "def": "hatred; antagonism",
                "eg1": "Of course we're competitive but there's no personal animosity between us.",
                "eg2": "Despite the betrayal, she bore her former friends no animosity."
            },
            {
                "word": "Unequivocal",
                "key": 16,
                "type": "adjective",
                "def": "clear; obvious; unambiguous",
                "eg1": "Answering in an unequivocal manner guarentees maximum marks.",
                "eg2": "The Prime Minister has been unequivocal in his condemnation of the violence."
            },
            {
                "word": "Dilettante",
                "key": 17,
                "type": "noun",
                "def": "person who dabbles in a subject without serious study",
                "eg1": "He's a bit of a dilettante as far as wine is concerned."
            },
            {
                "word": "Inductee",
                "key": 18,
                "type": "noun",
                "def": "novice; beginner",
                "eg1": "A new group of inductees were taken in by the army."
            },
            {
                "word": "Dike",
                "key": 19,
                "type": "noun",
                "def": "dam; embankment",
                "eg1": "The dike around the shop complex had broken."
            },
            {
                "word": "Anthology",
                "key": 20,
                "type": "noun",
                "def": "a collection of selected literary passages",
                "eg1": "We have put together an anthology of children's poetry."
            },
            {
                "word": "Indifferent",
                "key": 21,
                "type": "adjective",
                "def": "1) unconcerned; 2) mediocre",
                "eg1": "He appeared indifferent to her suffering.",
                "eg2": "These new reforms are very indifferent."
            },
            {
                "word": "Indelible",
                "key": 22,
                "type": "adjective",
                "def": "cannot be wiped out",
                "eg1": "His story made an indelible impression on me.",
                "eg2": "The blood had left an indelible mark on her shirt."
            },
            {
                "word": "Inconsequential",
                "key": 23,
                "type": "adjective",
                "def": "unimportant; insignificant; negligible",
                "eg1": "Most of what she said was pretty inconsequential.",
                "eg2": "This an inconsequential error that does nothing to lessen the value of the report."
            },
            {
                "word": "Parasite",
                "key": 24,
                "type": "noun",
                "def": "a follower who hangs around a host (without benefit to the host) in hope of gain or advantage",
                "eg1": "I don't want to be a parasite. I must earn my own way in life."
            },
            {
                "word": "Malinger",
                "key": 25,
                "type": "verb",
                "def": "avoid responsibilities and duties by pretending to be ill",
                "eg1": "I'm sure he's not malingering. He looked awful when I saw him last night.",
                "eg2": "Is he really ill or just malingering?"
            }
        ]
    },
    {
        "name": "Section 19",
        "key": "19",
        "length": 25,
        "words": [
            {
                "word": "Prodigal",
                "key": 1,
                "type": "adjective",
                "def": "wasteful; extravagant",
                "eg1": "Once developed, prodigal habits die hard.",
                "eg2": "The dessert was prodigal with whipped cream."
            },
            {
                "word": "Serene",
                "key": 2,
                "type": "adjective",
                "def": "calm; peaceful",
                "eg1": "Wow! What a serene sunset.",
                "eg2": "Her eyes were closed and she looked very serene."
            },
            {
                "word": "Unprecedented",
                "key": 3,
                "type": "adjective",
                "def": "never having happened before",
                "eg1": "Such policies would require unprecedented cooperation between nations.",
                "eg2": "The air crash caused an unprecedented number of deaths."
            },
            {
                "word": "Parched",
                "key": 4,
                "type": "adjective",
                "def": "dried up",
                "eg1": "The lack of rain had parched the land.",
                "eg2": "He raised the water bottle to his parched lips."
            },
            {
                "word": "Antagonistic",
                "key": 5,
                "type": "adjective",
                "def": "opposed; hostile; aggressive",
                "eg1": "He's always antagonistic towards new ideas.",
                "eg2": "Why are Kate and John so antagonistic towards each other?"
            },
            {
                "word": "Seminary",
                "key": 6,
                "type": "noun",
                "def": "an institution in which priests are trained",
                "eg1": "The seminary is of enormous symbolic importance."
            },
            {
                "word": "Extraneous",
                "key": 7,
                "type": "adjective",
                "def": "1) irrelevant; 2) of external origin",
                "eg1": "Such details are extraneous to the matter in hand.",
                "eg2": "When the transmitter pack is turned off no extraneous noise is heard."
            },
            {
                "word": "Indulgent",
                "key": 8,
                "type": "adjective",
                "def": "pampering; satisfying desires",
                "eg1": "Grandparents are usually very indulgent.",
                "eg2": "We shouldn't be indulgent towards his mistakes."
            },
            {
                "word": "Incongruous",
                "key": 9,
                "type": "adjective",
                "def": "not fitting in; out of place",
                "eg1": "The duffel coat looked incongruous with the black dress she wore underneath.",
                "eg2": "It was incongruous to see the policeman do drugs."
            },
            {
                "word": "Chicanery",
                "key": 10,
                "type": "noun",
                "def": "trickery",
                "eg1": "Let us expose that business for its financial chicanery.",
                "eg2": "He wasn't above using chicanery to win votes."
            },
            {
                "word": "Unfrock",
                "key": 11,
                "type": "verb",
                "def": "to deprive someone of rank or authority; depose.",
                "eg1": "If you do not immediately comply with my request, I will unfrock you."
            },
            {
                "word": "Profanity",
                "key": 12,
                "type": "noun",
                "def": "swearing; cursing",
                "eg1": "The comic uses too much profanity. Children should'nt read it.",
                "eg2": "The film contains profanity and violence."
            },
            {
                "word": "Indolence",
                "key": 13,
                "type": "noun",
                "def": "laziness",
                "eg1": "My failure is probably due to my own indolence.",
                "eg2": "After a sudden burst of activity, the team lapsed back into indolence."
            },
            {
                "word": "Fanaticism",
                "key": 14,
                "type": "noun",
                "def": "excessive intolerance of opposing views",
                "eg1": "The fanaticism of football fans is unopposed.",
                "eg2": "A Muslim cleric from France hopes to unite Muslims, Christians and Jews in the fight against fanaticism."
            },
            {
                "word": "Pallid",
                "key": 15,
                "type": "adjective",
                "def": "pale(of a person's face); lacking vigour(generic)",
                "eg1": "A pallid ray of winter sun fell upon the ground.",
                "eg2": "You look a bit pallid do you feel all right?"
            },
            {
                "word": "Anomaly",
                "key": 16,
                "type": "noun",
                "def": "something which does not fit in a pattern; irregularity",
                "eg1": "Tthere are a number of anomalies in the present system.",
                "eg2": "They regarded the test results as an anomaly."
            },
            {
                "word": "Pariah",
                "key": 17,
                "type": "noun",
                "def": "an outcast from society",
                "eg1": "The criminals were treated as social pariahs."
            },
            {
                "word": "Urbane",
                "key": 18,
                "type": "adjective",
                "def": "sophisticated; (of a person) courteous and refined in manner",
                "eg1": "He treated his guest in an urbane manner.",
                "eg2": "He maintained an urbane tone in his letters."
            },
            {
                "word": "Manipulatable",
                "key": 19,
                "type": "adjective",
                "def": "influencable; controllable",
                "eg1": "He is a very manipulable person.",
                "eg2": "The internet allows you to treat information as a manipulable thing."
            },
            {
                "word": "Fastidious",
                "key": 20,
                "type": "adjective",
                "def": "very attentive to and concerned about accuracy and detail",
                "eg1": "She dressed with fastidious care.",
                "eg2": "Everything was planned in fastidious detail."
            },
            {
                "word": "Sentinel",
                "key": 21,
                "type": "noun/verb",
                "def": "guard; sentry",
                "eg1": "The soldiers stood sentinel(noun) with their big AK47s.",
                "eg2": "The racecourse had been roped off and sentinelled(verb) with police."
            },
            {
                "word": "Scrupulous",
                "key": 22,
                "type": "adjective",
                "def": "careful; diligent; painstaking",
                "eg1": "The research has been carried out with scrupulous attention to detail.",
                "eg2": "You must be scrupulous about hygiene when you're handling a baby."
            },
            {
                "word": "Scrutinize",
                "key": 23,
                "type": "verb",
                "def": "examine or inspect closely and thoroughly",
                "eg1": "He scrutinized the men's faces carefully, trying to work out who was lying.",
                "eg2": "All new products are scrutinized by the laboratory."
            },
            {
                "word": "Chary",
                "key": 24,
                "type": "adjective",
                "def": "wary of; cautious about; reluctant to give",
                "eg1": "She had been chary of telling the whole truth.",
                "eg2": "The bank is chary of lending the company more money."
            }
        ]
    },
    {
        "name": "Section 20",
        "key": "20",
        "length": 25,
        "words": [
            {
                "word": "Unscathed",
                "key": 25,
                "type": "adjective",
                "def": "unharmed; intact; without a scratch",
                "eg1": "I survived the accident unscathed.",
                "eg2": "The company came through the crisis apparently unscathed."
            },
            {
                "word": "Scuttle",
                "key": 1,
                "type": "noun",
                "def": "to move about or proceed hurriedly",
                "eg1": "The pigeons wheel and scuttle around us when we give them food.",
                "eg2": "He watched a lizard scuttle furtively along the join between wall and ceiling."
            },
            {
                "word": "Paradigm",
                "key": 2,
                "type": "noun",
                "def": "a typical example of something",
                "eg1": "He had become the paradigm of the successful man.",
                "eg2": "The war was a paradigm of the evil and destructive side of human nature."
            },
            {
                "word": "Certitude",
                "key": 3,
                "type": "noun",
                "def": "certainty",
                "eg1": "Philosophical questions can never be answered with certitude.",
                "eg2": "I can say with certitude that I will marry her."
            },
            {
                "word": "Profane",
                "key": 4,
                "type": "adjective",
                "def": "unholy",
                "eg1": "The churchcondemned the use of profane language."
            },
            {
                "word": "Incoherent",
                "key": 5,
                "type": "adjective",
                "def": "expressed in an incomprehensible or confusing way; unclear",
                "eg1": "He was so drunk that heverything he muttered was incoherent.",
                "eg2": "He was confused and incoherent and I didn't get much sense out of him."
            },
            {
                "word": "Feasible",
                "key": 6,
                "type": "adjective",
                "def": "possible and practicable",
                "eg1": "The tunnel was not considered economically feasible.",
                "eg2": "Is it feasible to finish the work by Christmas?"
            },
            {
                "word": "Upbraid",
                "key": 7,
                "type": "verb",
                "def": "scold; tell off; reprimand",
                "eg1": "He was upbraided for his messy attire.",
                "eg2": "The captain upbraid his men for falling asleep."
            },
            {
                "word": "Antagonism",
                "key": 8,
                "type": "noun",
                "def": "hostility; strong opposition",
                "eg1": "There's a history of antagonism between the two teams.",
                "eg2": "The antagonism he felt towards his old enemy was still very strong."
            },
            {
                "word": "Sensuous",
                "key": 9,
                "type": "adjective",
                "def": "appealing to the senses",
                "eg1": "A gentle, sensuous breeze caressed our faces.",
                "eg2": "The sensuous sounds of soul music created a warm atmosphere."
            },
            {
                "word": "Censorious",
                "key": 10,
                "type": "adjective",
                "def": "disapproving; critical",
                "eg1": "Despite having strong principles, he was never censorious, just strict.",
                "eg2": "She fixed her daughter's behavious with a censorious eye."
            },
            {
                "word": "Chimerical",
                "key": 11,
                "type": "adjective",
                "def": "unreal; imaginary; visionary",
                "eg1": "He dreams of a chimerical terrestrial paradise.",
                "eg2": "Please don\u2019t make all these chimerical plan."
            },
            {
                "word": "Pristine",
                "key": 12,
                "type": "adjective",
                "def": "in its original condition; unspoilt; fresh",
                "eg1": "These are the only pristine copies of an early magazine.",
                "eg2": "Please wear a pristine white shirt to the meeting."
            },
            {
                "word": "Dilemma",
                "key": 13,
                "type": "noun",
                "def": "puzzling situation",
                "eg1": "I'm in a dilemma about this job offer.",
                "eg2": "She found herself in a dilemma when her two best friends were fighting."
            },
            {
                "word": "Unwitting",
                "key": 14,
                "type": "adjective",
                "def": "1) not aware of the full facts; 2) unintentional",
                "eg1": "He is an unwitting lawyer in relation to this case.",
                "eg2": "We are anxious to rectify the unwitting mistakes we made in the past."
            },
            {
                "word": "Censure",
                "key": 15,
                "type": "verb/noun",
                "def": "express severe disapproval of (someone or something)",
                "eg1": "The company was heavily censured by the health department.",
                "eg2": "Two MPs were singled out for censure(noun)."
            },
            {
                "word": "Palpable",
                "key": 16,
                "type": "adjective",
                "def": "easily felt; easily perceived",
                "eg1": "The tension between Shyamn and Ram is palpable.",
                "eg2": "There was a palpable sense of relief among the crowd."
            },
            {
                "word": "Annex",
                "key": 17,
                "type": "noun",
                "def": "an addition to something; an extra building",
                "eg1": "This is an annex to the report.",
                "eg2": "This is the school's one-storey wooden annex."
            },
            {
                "word": "Procrastinate",
                "key": 18,
                "type": "verb",
                "def": "delay; put off",
                "eg1": "People often procrastinate when it comes to important work.",
                "eg2": "Don't procrastinate until the day before a deadline, you will fail!"
            },
            {
                "word": "Extrinsic",
                "key": 19,
                "type": "adjective",
                "def": "irrelevant; on the outside",
                "eg1": "You have to consider all extrinsic factors before making business decisions.",
                "eg2": "A cash reward is an extrinsic motivation to study."
            },
            {
                "word": "Upshot",
                "key": 20,
                "type": "noun",
                "def": "outcome",
                "eg1": "The upshot of the meeting was that he was on the next plane to California.",
                "eg2": "The upshot of it all was that he resigned."
            },
            {
                "word": "Malady",
                "key": 21,
                "type": "noun",
                "def": "a disease or ailment; a serious problem",
                "eg1": "AIDS is considred to be an incurable malady.",
                "eg2": "The nation's maladies are more than we imagined."
            },
            {
                "word": "Mallet",
                "key": 22,
                "type": "noun",
                "def": "1. wooden hammer; 2. stick used for polo",
                "eg1": "The killer smashed the victim with a wooden mallet."
            },
            {
                "word": "Anthropocentrism",
                "key": 23,
                "type": "adjective",
                "def": "putting man at the center of one's philosophy",
                "eg1": "The ultimate goal is to question the anthropocentric, human-centered world view."
            },
            {
                "word": "Dilatory",
                "key": 24,
                "type": "adjective",
                "def": "slow; falling behind with one's work",
                "eg1": "He had been dilatory in appointing a successor.",
                "eg2": "The government has been dilatory in dealing with the problem of unemployment."
            },
            {
                "word": "Fanatical",
                "key": 25,
                "type": "adjective",
                "def": "obsessive; fixated",
                "eg1": "Her husband was fanatical about cleanliness.",
                "eg2": "She's fanatical about healthy eating."
            }
        ]
    }
];















// export const groups =  [
//     {
//         name:'Beginner 1',
//         key:'1',
//         length: 100,
//         words:[
//             {word: "implicit", key: '1', type: 'noun', def: 'suggested though not directly expressed.', eg1: 'comments seen as implicit criticism of the policies', eg2: 'the values implicit in the school ethos'},
//             {word: "explicit", key: '2', type: 'noun', def: 'suggested though not directly expressed.', eg1: 'comments seen as implicit criticism of the policies', eg2: 'the values implicit in the school ethos'},
//             {word: "explicit", key: '3', type: 'noun', def: 'suggested though not directly expressed.', eg1: 'comments seen as implicit criticism of the policies', eg2: 'the values implicit in the school ethos'},

//         ]
//     },
//     {
//         name:'Beginner 2',
//         key:'2',
//         length: 100,
//         words:[
//             {word: "implicit", key: '1', type: 'noun', def: 'suggested though not directly expressed.', eg1: 'comments seen as implicit criticism of the policies', eg2: 'the values implicit in the school ethos'},
//         ]
//     },
//   ];


