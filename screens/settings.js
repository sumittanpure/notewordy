import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {globalStyles} from '../style/globalStyles' 
import {MaterialIcons} from '@expo/vector-icons'
import colours from '../style/colours';

export default function Settings({navigation}){
    return (
        <View style= {styles.container}>
            {/* Heading */}
            <View style= {styles.heading}>
                <MaterialIcons
                    name="arrow-back"
                    size={32}
                    color="black"
                    style= {{marginRight: 12, marginTop: 8}}
                    onPress = {()=> navigation.goBack()}
                />
                <Text style= {globalStyles.heading1}>Settings</Text>
            </View>

            {/* About Us Card */}
            <TouchableOpacity onPress= {()=>navigation.navigate('AboutUs')}>
                <View style= {styles.settingCard}>
                    <View style= {{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                        <View style= {styles.square}>
                            <MaterialIcons name="info" size={32} color={colours.tertiary}/>
                        </View>
                        <Text style= {globalStyles.heading2}>About Us</Text>
                    </View>
                    <MaterialIcons name="chevron-right" size={32} color="black"/>
                </View>
            </TouchableOpacity>

            {/* Dark Mode Card */}
            {/* <View style= {styles.settingCard}>
                <View style= {{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                    <View style= {styles.square}>
                        <MaterialIcons name="wb-sunny" size={32} color={colours.primary1}/>
                    </View>
                    <Text style= {[globalStyles.heading2, {color: colours.primary2}]}>Dark Mode</Text>
                </View>
            </View> */}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingHorizontal: 16,
    },
    heading: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 24
    },
    square: {
        height: 56,
        width: 56,
        backgroundColor: colours.primary2,
        borderRadius: 16,
        marginRight: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    settingCard: {
        padding: 16,
        marginVertical: 12,
        flexDirection: 'row',
        alignItems: 'center'
    }
});