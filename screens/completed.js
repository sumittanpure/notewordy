import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, BackHandler } from 'react-native';
import { connect } from 'react-redux';
import { globalStyles } from '../style/globalStyles';
import {MaterialCommunityIcons} from '@expo/vector-icons'
import colours from '../style/colours';
import {groups} from '../data/words';
import { updateWords } from '../redux/actions';

function Completed({navigation, wordsLeft, updateWords}){

  const item = navigation.getParam('item');
  const index = navigation.getParam('index');

  return(
      <View style= {styles.container}>
        <View style= {styles.heading}>
            <Text style= {globalStyles.heading1}></Text>
            <MaterialCommunityIcons name="close" size={32} color="black" onPress= {()=> navigation.goBack()}/>
        </View>
        <View style= {{justifyContent: 'center', flex: 1}}>
          <Text style= {[globalStyles.heading1, {textAlign: 'center', paddingBottom: 16}]}>{item.name}</Text>
          <Text style= {[globalStyles.body1, {textAlign: 'center'}]}>Looks like you have already completed this section. Reset this section to revise again?</Text>
          <TouchableOpacity
              onPress= {()=>{
                var w = wordsLeft;
                w[index].words = groups[index].words;
                w[index].length = groups[index].length;
                updateWords(w, true);
                navigation.navigate('Home', {item: 'hello'});

              }}
          >
              <View style= {styles.yesButton}>
                  <MaterialCommunityIcons
                      name="restart"
                      size={32}
                      color= {colours.tertiary}
                  />
                  <View style= {{flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                      <Text style= {{color: colours.tertiary, fontFamily: 'oswald-bold', fontSize: 16}}>Reset section</Text>
                  </View>
              </View>
          </TouchableOpacity>
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,

  },
  yesButton: {
    marginVertical: 16,
    backgroundColor: colours.secondary,
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
  },
  heading: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
},
});

const mapStateToProps = state => {
  return {
    wordsLeft: state.wordsLeft
  }
}

const mapDispatchToProps = dispatch => {
  return{
    updateWords: (wordsLeft, isReset) => dispatch(updateWords(wordsLeft, isReset)),
  }
}

const connectComponent = connect(mapStateToProps, mapDispatchToProps);
export default connectComponent(Completed);