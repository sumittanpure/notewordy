import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Linking} from 'react-native'
import {MaterialCommunityIcons} from '@expo/vector-icons'
import { globalStyles } from '../style/globalStyles';
import colours from '../style/colours';

export default function AboutUs({navigation}){
    return (
        <View style= {styles.container}>
            <View style= {styles.heading}>
                <Text style= {globalStyles.heading1}></Text>
                <MaterialCommunityIcons name="close" size={32} color="black" onPress= {()=> navigation.goBack()}/>
            </View>
            <View style= {styles.mainSection}>
                <Text style= {[globalStyles.heading2, {marginBottom: 16, marginTop: 32}]}>About Us</Text>
                <Text style= {[globalStyles.body1, {letterSpacing: 1, lineHeight: 20, marginBottom: 16}]}>The developers of this app are three friend who wanted to come together and do something for all the students out there! We all know how hard it is to find an app which not only improves your vocabulary but which looks minimalist, is easy to use and enhances your learning process all at the same time. So we thought, "Why not just make one?". Our motto has always been 'By the students, for the students'! And we hope you enjoy this! :)</Text>
            </View>
            <TouchableOpacity
                    onPress= {()=>{
                        Linking.openURL('mailto:xmechapps@gmail.com?subject=Notewordy Feeback')
                    }}
                >
                    <View style= {styles.contactUs}>
                        <MaterialCommunityIcons
                            name="phone-in-talk"
                            size={32}
                            color= {colours.tertiary}
                        />
                        <View style= {{flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style= {{color: colours.tertiary, fontFamily: 'oswald-bold', fontSize: 16}}>Contact Us</Text>
                        </View>
                    </View>
                </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingHorizontal: 16,
  
    },
    mainSection: {
        flex: 1,
    },
    heading: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    contactUs: {
        marginVertical: 16,
        backgroundColor: colours.secondary,
        padding: 8,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 8,
    }
})