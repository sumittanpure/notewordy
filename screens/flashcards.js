import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, BackHandler} from 'react-native';
import {globalStyles} from '../style/globalStyles' 
import {MaterialIcons, MaterialCommunityIcons} from '@expo/vector-icons'
import colours from '../style/colours';
import Accordion from '../components/accordion';
import { nextWord } from '../data/logic';
import { updateWords } from '../redux/actions';
import { connect } from 'react-redux';

function FlashCards({navigation, wordsLeft, updateWords}){
    const item = navigation.getParam('item');
    const index = navigation.getParam('index');
    const words = item.words
    const [wordsRem, setWordsRem] = useState(words);
    const [i, setI] = useState(0);
    useEffect(()=>{
        if(wordsRem.length> 0){
            if(i >= wordsRem.length-1){
                setI(0);
            }
        }else{
            navigation.navigate('Home', {item: 'hello'})
        }

    }, [wordsRem]);

    BackHandler.addEventListener('hardwareBackPress', ()=> {
        navigation.navigate('Home', {item: 'hello'});
        return true;
    });

    return (
        <View style= {styles.container}>
            {/* Heading */}
            <View style= {styles.heading}>
                <MaterialIcons
                    name="arrow-back"
                    size={32}
                    color="black"
                    style= {{marginRight: 12, marginTop: 8}}
                    onPress = {()=> navigation.navigate('Home', {item: 'hello'})}
                />
                <Text style= {globalStyles.heading1}>{item.name}</Text>
            </View>

            {/* Word Card */}
            <View style= {styles.wordSpace}>
                <View style= {styles.wordCard}>
                    <Text style= {[globalStyles.heading2, {fontSize: 28}]}>{wordsRem.length>0?wordsRem[i].word: ''}</Text>
                    <Text style= {globalStyles.body2}>[{wordsRem.length>0?wordsRem[i].type: ''}]</Text>
                    <View style= {{flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 8}}>
                            <Text numberOfLines={1} style= {{color: colours.tertiary, fontFamily: 'oswald-bold', fontSize: 80, opacity: 0.05}}>{wordsRem.length>0?wordsRem[i].word:''}</Text>
                    </View>
                    <Accordion item= {wordsRem.length>0?wordsRem[i]: {}}/>
                </View>
            </View>

            {/* Buttons */}
            <View style={styles.buttons}>

                {/* Yes Button */}
                <TouchableOpacity
                    onPress= {()=>{
                        var w = wordsLeft;
                        w[index].words = wordsRem.filter(word => wordsRem.indexOf(word) != i);
                        w[index].length = w[index].length -1
                        updateWords(w);
                        setWordsRem(w[index].words);

                    }}
                >
                    <View style= {styles.yesButton}>
                        <MaterialCommunityIcons
                            name="check-circle-outline"
                            size={32}
                            color= {colours.tertiary}
                        />
                        <View style= {{flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style= {{color: colours.tertiary, fontFamily: 'oswald-bold', fontSize: 16}}>I know this already</Text>
                        </View>
                    </View>
                </TouchableOpacity>

                {/* No Button */}
                <TouchableOpacity
                    onPress= {()=>{
                        setI(
                            nextWord(i, wordsRem, navigation, 0))}
                    }
                >
                    <View style= {styles.noButton}>
                        <MaterialCommunityIcons
                            name="close-circle-outline"
                            size={32}
                            color= {colours.secondary}
                        />
                        <View style= {{flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style= {{color: colours.primary1, fontFamily: 'oswald-bold', fontSize: 16}}>I have no idea</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingHorizontal: 16,
    },
    heading: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    wordSpace: {
        marginTop: 40,
        flex: 1,
        justifyContent: 'flex-start'
    },
    wordCard: {
        backgroundColor: colours.primary1,
        borderRadius: 16,
        shadowColor: '#000000',
        elevation: 10,
        alignItems: 'center',
        paddingTop: 32,
    },
    buttons: {
        marginBottom: 32,
    },
    noButton: {
        backgroundColor: colours.tertiary,
        padding: 8,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 8,
    },
    yesButton: {
        marginVertical: 16,
        backgroundColor: colours.secondary,
        padding: 8,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 8,
    }
});

const mapStateToProps = state => {
    return {
      wordsLeft: state.wordsLeft
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return{
      updateWords: wordsLeft => dispatch(updateWords(wordsLeft)),
    }
  }
  
  const connectComponent = connect(mapStateToProps, mapDispatchToProps);
  export default connectComponent(FlashCards);