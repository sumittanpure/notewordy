import React from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {MaterialIcons} from '@expo/vector-icons'
import ExploreCards from '../components/explore_cards';
import ListCard from '../components/list_card';
import {globalStyles} from '../style/globalStyles'
import { updateWords } from '../redux/actions';
import { connect } from 'react-redux';

function Home({navigation, wordsLeft, wordsToday, wordsTotal}) {
  const settingsHandler = () => {
    navigation.navigate('Settings');
  }

  // const d = new Date();
  // console.log(d.getDate());

  return (
    <View style={styles.container}>

        {/* Heading */}
        <View style= {styles.heading}>
            <Text style= {globalStyles.heading1}>Explore</Text>
            <MaterialIcons name="settings" size={32} color="black" onPress= {settingsHandler}/>
        </View>

        {/* Explore Section */}
        <ExploreCards wordsToday= {wordsToday} wordsTotal= {wordsTotal}/>

        {/* List View */}
        <View style= {styles.heading, {marginTop: 32, flex: 1}}>
          <Text style= {globalStyles.heading1}>Collections</Text>
          <FlatList
            data= {wordsLeft}
            renderItem= {({item, index}) =>(
              <TouchableOpacity onPress= {()=> item.words.length> 0? navigation.navigate('FlashCards', {item: item, index: index}): navigation.navigate('Completed', {item: item, index: index})}>
                <ListCard item={item}/>
              </TouchableOpacity>
            )}
          />
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  heading: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
  },
  title: {
      flexDirection: 'row'
  }
});

const mapStateToProps = state => {
  return {
    wordsLeft: state.wordsLeft,
    wordsToday: state.wordsToday,
    wordsTotal: state.wordsTotal
  }
}

const mapDispatchToProps = dispatch => {
  return{
    updateWords: wordsLeft => dispatch(updateWords(wordsLeft))
  }
}

const connectComponent = connect(mapStateToProps, mapDispatchToProps);
export default connectComponent(Home);