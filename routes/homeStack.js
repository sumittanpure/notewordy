import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Home from '../screens/home';
import FlashCards from '../screens/flashcards';
import Settings from '../screens/settings';
import Completed from "../screens/completed";
import AboutUs from "../screens/about_us";

const screens = {
    Home: {
        screen: Home,
        navigationOptions: {
            headerShown: false
        }
    },
    FlashCards: {
        screen: FlashCards,
        navigationOptions: {
            headerShown: false
        }
    },
    Settings: {
        screen: Settings,
        navigationOptions: {
            headerShown: false
        }
    },
    AboutUs: {
        screen: AboutUs,
        navigationOptions: {
            headerShown: false
        }
    },
    Completed: {
        screen: Completed,
        navigationOptions: {
            headerShown: false
        }
    },
}

const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);