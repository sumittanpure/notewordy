import React, { useState } from 'react';
import * as Fonts from 'expo-font';
import AppLoading from 'expo-app-loading';
import { SafeAreaView, StyleSheet } from 'react-native';
import Navigator from './routes/homeStack';
import { Provider } from 'react-redux';
import store from './redux/store';

const getFonts = () => Fonts.loadAsync({
        'oswald-bold': require('./assets/fonts/Oswald-Bold.ttf'),
        'oswald-regular': require('./assets/fonts/Oswald-Regular.ttf'),
        'oswald-light': require('./assets/fonts/Oswald-Light.ttf'),
        'body-bold': require('./assets/fonts/Quattrocento-Bold.ttf'),
        'body-regular': require('./assets/fonts/Quattrocento-Regular.ttf'),
    });

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  if(fontsLoaded){
    return (
      <Provider store = {store}>
        <SafeAreaView style= {styles.droidSafeArea}>
          <Navigator/>
        </SafeAreaView>
      </Provider>
      
    );
  }else{
    return(
      <AppLoading
        startAsync= {getFonts}
        onFinish= {() => setFontsLoaded(true)}
        onError= {console.warn}
      />
    );
  }
}

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? 49 : 24,
    backgroundColor: '#F0F0F0'
},
});
