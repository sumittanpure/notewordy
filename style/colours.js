export default Colours = {
    primary1: '#F0F0F0',
    primary2: '#E2E2E2',
    secondary: '#FED700',
    tertiary: '#282828',
    dark: '#4F4F4F'
}