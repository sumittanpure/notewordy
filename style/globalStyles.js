import {StyleSheet} from 'react-native';

export const globalStyles = StyleSheet.create({
    heading1: {
        fontFamily: 'oswald-bold',
        fontSize: 32,
    },
    heading2: {
        fontFamily: 'oswald-bold',
        fontSize: 24,
        marginBottom: 2
    },
    body1: {
        fontFamily: 'body-regular',
        fontSize: 16,
        marginBottom: 8
    },
    body2: {
        fontFamily: 'body-bold',
        fontSize: 16,
        marginBottom: 8
    }
});