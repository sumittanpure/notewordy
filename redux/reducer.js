import { groups } from "../data/words";
import { UPDATE_DATA, UPDATE_WORDS } from "./types";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { updateData} from '../redux/actions';

const initialState = {
    wordsLeft: groups,
    wordsToday: {
        words: 0,
        date: 'Thu Apr 12 2018',
    },
    wordsTotal: 0
}

const asyncLoad = async(action, state) => {
    if(action.isReset){
        const data = {wordsLeft: action.payload, wordsTotal: state.wordsTotal-25 >= 25 ?state.wordsTotal-25: 0, wordsToday: sameDate(state.wordsToday, 1, true)}
        const jsonValue = JSON.stringify(data);
        await AsyncStorage.setItem('wordsRemaining', jsonValue);
    }else{
        const data = {wordsLeft: action.payload, wordsTotal: state.wordsTotal + 1, wordsToday: sameDate(state.wordsToday, 1, false)}
        const jsonValue = JSON.stringify(data);
        await AsyncStorage.setItem('wordsRemaining', jsonValue);
    }
}

const sameDate = (wordsToday, num, isReset)=> {
    const date = new Date();
    const day = date.toDateString();
    if(day != wordsToday.date){
        if(num == 1){
            return{
                date: day,
                words: 1
            }
        }else{
            return{
                date: day,
                words: 0
            }
        }
    }else{
        if(num == 1){
            if(isReset){
                return{
                    date: day,
                    words: wordsToday.words
                }
            }else{
                return{
                    date: day,
                    words: wordsToday.words +1
                }
            }
        }else{
            return{
                date: day,
                words: wordsToday.words
            }
        }
    }
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case UPDATE_WORDS:
            // console.log(action);
            asyncLoad(action, state);
            if(action.isReset){
                return{
                    wordsLeft: action.payload,
                    wordsTotal: state.wordsTotal-25 >= 25 ?state.wordsTotal-25: 0,
                    wordsToday: sameDate(state.wordsToday, 1, true)
                }
            }else{
                return{
                    wordsLeft: action.payload,
                    wordsTotal: state.wordsTotal +1,
                    wordsToday: sameDate(state.wordsToday, 1, false)
                }
            }
        case UPDATE_DATA:
            return {
                wordsLeft: action.payload.wordsLeft,
                wordsTotal: action.payload.wordsTotal,
                wordsToday: sameDate(action.payload.wordsToday, 0)
            }
        default:
            return state;
    }
}

export const getAsyncStorage = () => {
    return (dispatch) => {
      AsyncStorage.getItem('wordsRemaining')
      .then((result) => {
          if(result!= null){
            const output = JSON.parse(result);
            dispatch(updateData(output))
          }
    }).catch(err=> console.log(err));
    };
};

export default reducer;