import { createStore, applyMiddleware } from "redux";
import reducer from "./reducer";
import { getAsyncStorage } from "./reducer";
import thunk from 'redux-thunk';


const store = createStore(reducer, applyMiddleware(thunk));

store.dispatch(getAsyncStorage())

export default store;