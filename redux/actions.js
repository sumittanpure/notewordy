import { UPDATE_DATA, UPDATE_WORDS} from './types'

export const updateWords = (group, isReset= false) => {
    return{
        type: UPDATE_WORDS,
        payload: group,
        isReset: isReset
    }
}

export const updateData = out => {
    return{
        type: UPDATE_DATA,
        payload: out
    }
}